// Code generated by ent, DO NOT EDIT.

package ent

import (
	"fmt"
	"strings"
	"time"

	"entgo.io/ent"
	"entgo.io/ent/dialect/sql"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/ent/product"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/ent/productevent"
	uuid "github.com/gofrs/uuid/v5"
)

// ProductEvent is the model entity for the ProductEvent schema.
type ProductEvent struct {
	config `json:"-"`
	// ID of the ent.
	ID uint64 `json:"id,omitempty"`
	// Create Time | 创建日期
	CreatedAt time.Time `json:"created_at,omitempty"`
	// Update Time | 修改日期
	UpdatedAt time.Time `json:"updated_at,omitempty"`
	// Status 1: normal 2: ban | 状态 1 正常 2 禁用
	Status uint8 `json:"status,omitempty"`
	// Sort Number | 排序编号
	Sort uint32 `json:"sort,omitempty"`
	// Product Key | 产品Key
	ProductKey string `json:"productKey,omitempty"`
	// CreateTs | 功能创建的时间戳
	CreateTs int64 `json:"createTs,omitempty"`
	// Version | 物模型版本号
	Version string `json:"version,omitempty"`
	// Identifier | 事件的标识符
	Identifier string `json:"identifier,omitempty"`
	// EventName | 事件名称
	EventName string `json:"eventName,omitempty"`
	// EventType | 事件类型,INFO_EVENT_TYPE：信息。ALERT_EVENT_TYPE：告警。ERROR_EVENT_TYPE：故障。
	EventType string `json:"eventType,omitempty"`
	// Output data | 事件的输出参数
	OutputData string `json:"outputData,omitempty"`
	// Custom | 是否是自定义功能
	Custom bool `json:"custom,omitempty"`
	// Required | 是否是标准品类的必选事件
	Required bool `json:"required,omitempty"`
	// Edges holds the relations/edges for other nodes in the graph.
	// The values are being populated by the ProductEventQuery when eager-loading is set.
	Edges          ProductEventEdges `json:"edges"`
	product_events *uuid.UUID
	selectValues   sql.SelectValues
}

// ProductEventEdges holds the relations/edges for other nodes in the graph.
type ProductEventEdges struct {
	// Product holds the value of the product edge.
	Product *Product `json:"product,omitempty"`
	// loadedTypes holds the information for reporting if a
	// type was loaded (or requested) in eager-loading or not.
	loadedTypes [1]bool
}

// ProductOrErr returns the Product value or an error if the edge
// was not loaded in eager-loading, or loaded but was not found.
func (e ProductEventEdges) ProductOrErr() (*Product, error) {
	if e.Product != nil {
		return e.Product, nil
	} else if e.loadedTypes[0] {
		return nil, &NotFoundError{label: product.Label}
	}
	return nil, &NotLoadedError{edge: "product"}
}

// scanValues returns the types for scanning values from sql.Rows.
func (*ProductEvent) scanValues(columns []string) ([]any, error) {
	values := make([]any, len(columns))
	for i := range columns {
		switch columns[i] {
		case productevent.FieldCustom, productevent.FieldRequired:
			values[i] = new(sql.NullBool)
		case productevent.FieldID, productevent.FieldStatus, productevent.FieldSort, productevent.FieldCreateTs:
			values[i] = new(sql.NullInt64)
		case productevent.FieldProductKey, productevent.FieldVersion, productevent.FieldIdentifier, productevent.FieldEventName, productevent.FieldEventType, productevent.FieldOutputData:
			values[i] = new(sql.NullString)
		case productevent.FieldCreatedAt, productevent.FieldUpdatedAt:
			values[i] = new(sql.NullTime)
		case productevent.ForeignKeys[0]: // product_events
			values[i] = &sql.NullScanner{S: new(uuid.UUID)}
		default:
			values[i] = new(sql.UnknownType)
		}
	}
	return values, nil
}

// assignValues assigns the values that were returned from sql.Rows (after scanning)
// to the ProductEvent fields.
func (pe *ProductEvent) assignValues(columns []string, values []any) error {
	if m, n := len(values), len(columns); m < n {
		return fmt.Errorf("mismatch number of scan values: %d != %d", m, n)
	}
	for i := range columns {
		switch columns[i] {
		case productevent.FieldID:
			value, ok := values[i].(*sql.NullInt64)
			if !ok {
				return fmt.Errorf("unexpected type %T for field id", value)
			}
			pe.ID = uint64(value.Int64)
		case productevent.FieldCreatedAt:
			if value, ok := values[i].(*sql.NullTime); !ok {
				return fmt.Errorf("unexpected type %T for field created_at", values[i])
			} else if value.Valid {
				pe.CreatedAt = value.Time
			}
		case productevent.FieldUpdatedAt:
			if value, ok := values[i].(*sql.NullTime); !ok {
				return fmt.Errorf("unexpected type %T for field updated_at", values[i])
			} else if value.Valid {
				pe.UpdatedAt = value.Time
			}
		case productevent.FieldStatus:
			if value, ok := values[i].(*sql.NullInt64); !ok {
				return fmt.Errorf("unexpected type %T for field status", values[i])
			} else if value.Valid {
				pe.Status = uint8(value.Int64)
			}
		case productevent.FieldSort:
			if value, ok := values[i].(*sql.NullInt64); !ok {
				return fmt.Errorf("unexpected type %T for field sort", values[i])
			} else if value.Valid {
				pe.Sort = uint32(value.Int64)
			}
		case productevent.FieldProductKey:
			if value, ok := values[i].(*sql.NullString); !ok {
				return fmt.Errorf("unexpected type %T for field productKey", values[i])
			} else if value.Valid {
				pe.ProductKey = value.String
			}
		case productevent.FieldCreateTs:
			if value, ok := values[i].(*sql.NullInt64); !ok {
				return fmt.Errorf("unexpected type %T for field createTs", values[i])
			} else if value.Valid {
				pe.CreateTs = value.Int64
			}
		case productevent.FieldVersion:
			if value, ok := values[i].(*sql.NullString); !ok {
				return fmt.Errorf("unexpected type %T for field version", values[i])
			} else if value.Valid {
				pe.Version = value.String
			}
		case productevent.FieldIdentifier:
			if value, ok := values[i].(*sql.NullString); !ok {
				return fmt.Errorf("unexpected type %T for field identifier", values[i])
			} else if value.Valid {
				pe.Identifier = value.String
			}
		case productevent.FieldEventName:
			if value, ok := values[i].(*sql.NullString); !ok {
				return fmt.Errorf("unexpected type %T for field eventName", values[i])
			} else if value.Valid {
				pe.EventName = value.String
			}
		case productevent.FieldEventType:
			if value, ok := values[i].(*sql.NullString); !ok {
				return fmt.Errorf("unexpected type %T for field eventType", values[i])
			} else if value.Valid {
				pe.EventType = value.String
			}
		case productevent.FieldOutputData:
			if value, ok := values[i].(*sql.NullString); !ok {
				return fmt.Errorf("unexpected type %T for field outputData", values[i])
			} else if value.Valid {
				pe.OutputData = value.String
			}
		case productevent.FieldCustom:
			if value, ok := values[i].(*sql.NullBool); !ok {
				return fmt.Errorf("unexpected type %T for field custom", values[i])
			} else if value.Valid {
				pe.Custom = value.Bool
			}
		case productevent.FieldRequired:
			if value, ok := values[i].(*sql.NullBool); !ok {
				return fmt.Errorf("unexpected type %T for field required", values[i])
			} else if value.Valid {
				pe.Required = value.Bool
			}
		case productevent.ForeignKeys[0]:
			if value, ok := values[i].(*sql.NullScanner); !ok {
				return fmt.Errorf("unexpected type %T for field product_events", values[i])
			} else if value.Valid {
				pe.product_events = new(uuid.UUID)
				*pe.product_events = *value.S.(*uuid.UUID)
			}
		default:
			pe.selectValues.Set(columns[i], values[i])
		}
	}
	return nil
}

// Value returns the ent.Value that was dynamically selected and assigned to the ProductEvent.
// This includes values selected through modifiers, order, etc.
func (pe *ProductEvent) Value(name string) (ent.Value, error) {
	return pe.selectValues.Get(name)
}

// QueryProduct queries the "product" edge of the ProductEvent entity.
func (pe *ProductEvent) QueryProduct() *ProductQuery {
	return NewProductEventClient(pe.config).QueryProduct(pe)
}

// Update returns a builder for updating this ProductEvent.
// Note that you need to call ProductEvent.Unwrap() before calling this method if this ProductEvent
// was returned from a transaction, and the transaction was committed or rolled back.
func (pe *ProductEvent) Update() *ProductEventUpdateOne {
	return NewProductEventClient(pe.config).UpdateOne(pe)
}

// Unwrap unwraps the ProductEvent entity that was returned from a transaction after it was closed,
// so that all future queries will be executed through the driver which created the transaction.
func (pe *ProductEvent) Unwrap() *ProductEvent {
	_tx, ok := pe.config.driver.(*txDriver)
	if !ok {
		panic("ent: ProductEvent is not a transactional entity")
	}
	pe.config.driver = _tx.drv
	return pe
}

// String implements the fmt.Stringer.
func (pe *ProductEvent) String() string {
	var builder strings.Builder
	builder.WriteString("ProductEvent(")
	builder.WriteString(fmt.Sprintf("id=%v, ", pe.ID))
	builder.WriteString("created_at=")
	builder.WriteString(pe.CreatedAt.Format(time.ANSIC))
	builder.WriteString(", ")
	builder.WriteString("updated_at=")
	builder.WriteString(pe.UpdatedAt.Format(time.ANSIC))
	builder.WriteString(", ")
	builder.WriteString("status=")
	builder.WriteString(fmt.Sprintf("%v", pe.Status))
	builder.WriteString(", ")
	builder.WriteString("sort=")
	builder.WriteString(fmt.Sprintf("%v", pe.Sort))
	builder.WriteString(", ")
	builder.WriteString("productKey=")
	builder.WriteString(pe.ProductKey)
	builder.WriteString(", ")
	builder.WriteString("createTs=")
	builder.WriteString(fmt.Sprintf("%v", pe.CreateTs))
	builder.WriteString(", ")
	builder.WriteString("version=")
	builder.WriteString(pe.Version)
	builder.WriteString(", ")
	builder.WriteString("identifier=")
	builder.WriteString(pe.Identifier)
	builder.WriteString(", ")
	builder.WriteString("eventName=")
	builder.WriteString(pe.EventName)
	builder.WriteString(", ")
	builder.WriteString("eventType=")
	builder.WriteString(pe.EventType)
	builder.WriteString(", ")
	builder.WriteString("outputData=")
	builder.WriteString(pe.OutputData)
	builder.WriteString(", ")
	builder.WriteString("custom=")
	builder.WriteString(fmt.Sprintf("%v", pe.Custom))
	builder.WriteString(", ")
	builder.WriteString("required=")
	builder.WriteString(fmt.Sprintf("%v", pe.Required))
	builder.WriteByte(')')
	return builder.String()
}

// ProductEvents is a parsable slice of ProductEvent.
type ProductEvents []*ProductEvent
