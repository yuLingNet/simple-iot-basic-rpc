package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/dialect/entsql"
	"entgo.io/ent/schema"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
	"entgo.io/ent/schema/index"
	"github.com/suyuan32/simple-admin-common/orm/ent/mixins"
)

// Product holds the schema definition for the Product entity.
type Product struct {
	ent.Schema
}

// Fields of the Product.
func (Product) Fields() []ent.Field {
	return []ent.Field{
		field.String("productKey").
			Comment("Product Key | 产品Key").
			Annotations(entsql.WithComments(true)),
		field.String("productName").
			Comment("Product Name | 产品名称").
			Annotations(entsql.WithComments(true)),
		field.String("authType").
			Optional().
			Comment("Auth Type | 认证方式[secret,id2,x509]").
			Annotations(entsql.WithComments(true)),
		field.Int32("dataFormat").
			Optional().
			Comment("Data Format | 数据通信协议类型[0：透传模式。1：Alink JSON。]").
			Annotations(entsql.WithComments(true)),
		field.String("description").
			Optional().
			Comment("Description | 产品描述").
			Annotations(entsql.WithComments(true)),
		field.Int32("deviceCount").
			Default(0).
			Comment("Device Count | 设备数量").
			Annotations(entsql.WithComments(true)),
		field.Int64("gmtCreate").
			Optional().
			Comment("GMT Create | GMT创建时间").
			Annotations(entsql.WithComments(true)),
		field.Int32("nodeType").
			Default(0).
			Comment("Node Type | 节点类型[0：设备。1：网关]").
			Annotations(entsql.WithComments(true)),
		field.String("aliyunCommodityCode").
			Optional().
			Comment("Commodity Code | 产品类型，决定是否使用物模型功能。iothub_senior：使用物模型。iothub：不使用物模型。").
			Annotations(entsql.WithComments(true)),
		field.String("categoryKey").
			Optional().
			Comment("Category Key | 所属品类标识").
			Annotations(entsql.WithComments(true)),
		field.String("categoryName").
			Optional().
			Comment("Category Name | 所属品类名称").
			Annotations(entsql.WithComments(true)),
		field.Bool("id2").
			Optional().
			Default(false).
			Comment("ID² | 该产品是否使用ID²认证").
			Annotations(entsql.WithComments(true)),
		field.Int32("netType").
			Optional().
			Comment("Net Type | 产品下设备的联网方式").
			Annotations(entsql.WithComments(true)),
		field.Bool("owner").
			Optional().
			Default(false).
			Comment("Owner | 产品下设备的联网方式").
			Annotations(entsql.WithComments(true)),
		field.String("productSecret").
			Optional().
			Comment("Product Secret | 产品密钥").
			Annotations(entsql.WithComments(true)),
		field.String("productStatus").
			Optional().
			Comment("Product Status | 产品状态").
			Annotations(entsql.WithComments(true)),
		field.String("protocolType").
			Optional().
			Comment("Protocol Type | 子设备接入网关的协议类型").
			Annotations(entsql.WithComments(true)),
		field.Int32("validateType").
			Optional().
			Comment("Validate Type | 数据校验级别").
			Annotations(entsql.WithComments(true)),
	}
}

// Edges of the Product.
func (Product) Edges() []ent.Edge {
	return []ent.Edge{
		edge.To("devices", Device.Type),
		edge.To("tags", ProductTag.Type),
		edge.To("properties", ProductProperty.Type),
		edge.To("events", ProductEvent.Type),
		edge.To("services", ProductService.Type),
	}
}

// Mixin of the Product.
func (Product) Mixin() []ent.Mixin {
	return []ent.Mixin{
		mixins.UUIDMixin{},
		mixins.StatusMixin{},
		mixins.SortMixin{},
	}
}

// Indexes of the Product.
func (Product) Indexes() []ent.Index {
	return []ent.Index{
		index.Fields("productKey").Unique(),
	}
}

// Annotations of the Product
func (Product) Annotations() []schema.Annotation {
	return []schema.Annotation{
		entsql.Annotation{Table: "equip_products"},
	}
}
