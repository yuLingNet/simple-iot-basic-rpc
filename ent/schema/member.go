package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/dialect/entsql"
	"entgo.io/ent/schema"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
	"entgo.io/ent/schema/index"
	"github.com/suyuan32/simple-admin-common/orm/ent/mixins"
)

// Member holds the schema definition for the Member entity.
type Member struct {
	ent.Schema
}

// Fields of the Member.
func (Member) Fields() []ent.Field {
	return []ent.Field{
		field.Uint64("companyID").
			Comment("Company Id | 公司ID").
			Annotations(entsql.WithComments(true)),
		field.Int32("type").
			Optional().
			Default(0).
			Comment("Type | 类型").
			Annotations(entsql.WithComments(true)),
		field.String("name").
			Comment("Name | 姓名").
			Annotations(entsql.WithComments(true)),
		field.String("mobile").
			Comment("Mobile | 手机号码").
			Annotations(entsql.WithComments(true)),
		field.String("avatar").
			Optional().
			Comment("Avatar | 头像").
			Annotations(entsql.WithComments(true)),
		field.String("account").
			Unique().
			Comment("Account | 账号").
			Annotations(entsql.WithComments(true)),
		field.String("password").
			Comment("Password | 密码").
			Annotations(entsql.WithComments(true)),
		field.String("countryCode").
			Optional().
			Default("+86").
			Comment("Country Code | 国际区号").
			Annotations(entsql.WithComments(true)),
	}
}

// Edges of the Member.
func (Member) Edges() []ent.Edge {
	return []ent.Edge{
		edge.To("devices", Device.Type),
		edge.From("companies", Company.Type).Ref("members").Unique(),
	}
}

// Mixin of the Member.
func (Member) Mixin() []ent.Mixin {
	return []ent.Mixin{
		mixins.UUIDMixin{},
		mixins.StatusMixin{},
		mixins.SortMixin{},
	}
}

// Indexes of the Member.
func (Member) Indexes() []ent.Index {
	return []ent.Index{
		index.Fields("name", "mobile", "account").Unique(),
	}
}

// Annotations of the Member
func (Member) Annotations() []schema.Annotation {
	return []schema.Annotation{
		entsql.Annotation{Table: "client_members"},
	}
}
