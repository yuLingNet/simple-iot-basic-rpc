package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/dialect/entsql"
	"entgo.io/ent/schema"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
	"entgo.io/ent/schema/index"
	"github.com/suyuan32/simple-admin-common/orm/ent/mixins"
)

// Device holds the schema definition for the Device entity.
type Device struct {
	ent.Schema
}

// Fields of the Device.
func (Device) Fields() []ent.Field {
	return []ent.Field{
		field.String("productKey").
			Comment("Product Key | 产品Key").
			Annotations(entsql.WithComments(true)),
		field.String("iotID").
			Unique().
			Comment("Iot Id | 设备ID").
			Annotations(entsql.WithComments(true)),
		field.String("deviceName").
			Comment("Device Name | 设备名称").
			Annotations(entsql.WithComments(true)),
		field.String("deviceSecret").
			Optional().
			Comment("Device Secret | 设备密钥").
			Annotations(entsql.WithComments(true)),
		field.String("deviceStatus").
			Optional().
			Comment("Device Status | 设备状态").
			Annotations(entsql.WithComments(true)),
		field.String("nickname").
			Optional().
			Comment("Nickname | 备注名称").
			Annotations(entsql.WithComments(true)),
		field.String("gmtCreate").
			Optional().
			Comment("GMT Create | GMT创建时间").
			Annotations(entsql.WithComments(true)),
		field.String("gmtModified").
			Optional().
			Comment("GMT Modified | GMT更新时间").
			Annotations(entsql.WithComments(true)),
		field.String("utcCreate").
			Optional().
			Comment("UTC Create | UTC创建时间").
			Annotations(entsql.WithComments(true)),
		field.String("utcModified").
			Optional().
			Comment("UTC Modified | UTC更新时间").
			Annotations(entsql.WithComments(true)),
		field.String("operationalStatusCode").
			Optional().
			Comment("Operational Status Code | 操作状态码").
			Annotations(entsql.WithComments(true)),
		field.String("operationalStatus").
			Optional().
			Default("").
			Comment("Operational Status | 设备实时操作状态").
			Annotations(entsql.WithComments(true)),
		field.Uint64("tag").
			Optional().
			Comment("Tag | 标签").
			Annotations(entsql.WithComments(true)),
		field.String("firmwareVersion").
			Optional().
			Comment("Firmware Version | 固件版本号").
			Annotations(entsql.WithComments(true)),
		field.String("gmtActive").
			Optional().
			Comment("GMT Active | GMT激活时间").
			Annotations(entsql.WithComments(true)),
		field.String("gmtOnline").
			Optional().
			Comment("GMT Online | GMT最近一次上线的时间").
			Annotations(entsql.WithComments(true)),
		field.String("IPAddress").
			Optional().
			Comment("Ip Address | IP地址").
			Annotations(entsql.WithComments(true)),
		field.Int32("nodeType").
			Default(0).
			Comment("Node Type | 节点类型").
			Annotations(entsql.WithComments(true)),
		field.Bool("owner").
			Optional().
			Default(false).
			Comment("Owner | API调用者是否是该设备的拥有者").
			Annotations(entsql.WithComments(true)),
		field.String("productName").
			Optional().
			Comment("Product Name | 产品名称").
			Annotations(entsql.WithComments(true)),
		field.String("region").
			Optional().
			Comment("Region | 所在地区").
			Annotations(entsql.WithComments(true)),
		field.String("utcActive").
			Optional().
			Comment("UTC Active | UTC激活时间").
			Annotations(entsql.WithComments(true)),
		field.String("utcOnline").
			Optional().
			Comment("UTC Online | UTC最近一次上线的时间").
			Annotations(entsql.WithComments(true)),
	}
}

// Edges of the Device.
func (Device) Edges() []ent.Edge {
	return []ent.Edge{
		edge.From("product", Product.Type).Ref("devices").Unique(),
		edge.To("prop", DeviceProp.Type).Unique(),
		edge.From("companies", Company.Type).Ref("devices"),
		edge.From("members", Member.Type).Ref("devices"),
	}
}

// Mixin of the Device.
func (Device) Mixin() []ent.Mixin {
	return []ent.Mixin{
		mixins.UUIDMixin{},
		mixins.StatusMixin{},
		mixins.SortMixin{},
	}
}

// Indexes of the Device.
func (Device) Indexes() []ent.Index {
	return []ent.Index{
		index.Fields("productKey", "iotID", "deviceName").Unique(),
	}
}

// Annotations of the Device
func (Device) Annotations() []schema.Annotation {
	return []schema.Annotation{
		entsql.Annotation{Table: "equip_devices"},
	}
}
