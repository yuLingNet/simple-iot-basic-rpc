package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/dialect/entsql"
	"entgo.io/ent/schema"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
	"entgo.io/ent/schema/index"
	"github.com/suyuan32/simple-admin-common/orm/ent/mixins"
)

// DeviceProp holds the schema definition for the DeviceProp entity.
type DeviceProp struct {
	ent.Schema
}

// Fields of the DeviceProp.
func (DeviceProp) Fields() []ent.Field {
	return []ent.Field{
		field.String("iotID").
			Comment("Iot Id | 设备ID").
			Annotations(entsql.WithComments(true)),
		field.String("props").
			Comment("Props | 标签信息").
			Annotations(entsql.WithComments(true)),
	}
}

// Edges of the DeviceProp.
func (DeviceProp) Edges() []ent.Edge {
	return []ent.Edge{
		edge.From("device", Device.Type).Ref("prop").Unique(),
	}
}

// Mixin of the DeviceProp.
func (DeviceProp) Mixin() []ent.Mixin {
	return []ent.Mixin{
		mixins.IDMixin{},
		mixins.StatusMixin{},
		mixins.SortMixin{},
	}
}

// Indexes of the DeviceProp.
func (DeviceProp) Indexes() []ent.Index {
	return []ent.Index{
		index.Fields("iotID", "props").Unique(),
	}
}

// Annotations of the DeviceProp
func (DeviceProp) Annotations() []schema.Annotation {
	return []schema.Annotation{
		entsql.Annotation{Table: "equip_device_props"},
	}
}
