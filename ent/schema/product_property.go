package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/dialect/entsql"
	"entgo.io/ent/schema"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
	"entgo.io/ent/schema/index"
	"github.com/suyuan32/simple-admin-common/orm/ent/mixins"
)

// ProductProperty holds the schema definition for the ProductProperty entity.
type ProductProperty struct {
	ent.Schema
}

// Fields of the ProductProperty.
func (ProductProperty) Fields() []ent.Field {
	return []ent.Field{
		field.String("productKey").
			Comment("Product Key | 产品Key").
			Annotations(entsql.WithComments(true)),
		field.String("version").
			Optional().
			Comment("Version | 物模型版本号").
			Annotations(entsql.WithComments(true)),
		field.Int64("createTs").
			Optional().
			Comment("CreateTs | 功能创建的时间戳").
			Annotations(entsql.WithComments(true)),
		field.String("identifier").
			Comment("Identifier | 属性的标识符").
			Annotations(entsql.WithComments(true)),
		field.String("name").
			Comment("Name | 属性名称").
			Annotations(entsql.WithComments(true)),
		field.String("rwFlag").
			Default("READ_WRITE").
			Comment("RWFlag | 在云端可以对该属性进行的操作类型").
			Annotations(entsql.WithComments(true)),
		field.Bool("required").
			Default(false).
			Comment("Required | 是否是标准品类的必选属性").
			Annotations(entsql.WithComments(true)),
		field.String("dataType").
			Optional().
			Comment("Data Type | 属性值的数据类型,可选值：ARRAY、STRUCT、INT、FLOAT、DOUBLE、TEXT、DATE、ENUM、BOOL。").
			Annotations(entsql.WithComments(true)),
		field.Text("dataSpecs").
			Optional().
			Comment("Data Specs | 数据类型（dataType）为非列表型（INT、FLOAT、DOUBLE、TEXT、DATE、ARRAY）的数据规范存储在dataSpecs中").
			Annotations(entsql.WithComments(true)),
		field.Text("dataSpecsList").
			Optional().
			Comment("Data Specs List | 数据类型（dataType）为列表型（ENUM、BOOL、STRUCT）的数据规范存储在dataSpecsList中").
			Annotations(entsql.WithComments(true)),
		field.Bool("custom").
			Default(true).
			Comment("Custom | 是否是自定义功能").
			Annotations(entsql.WithComments(true)),
	}
}

// Edges of the ProductProperty.
func (ProductProperty) Edges() []ent.Edge {
	return []ent.Edge{
		edge.From("product", Product.Type).Ref("properties").Unique(),
	}
}

// Mixin of the ProductProperty.
func (ProductProperty) Mixin() []ent.Mixin {
	return []ent.Mixin{
		mixins.IDMixin{},
		mixins.StatusMixin{},
		mixins.SortMixin{},
	}
}

// Indexes of the ProductProperty.
func (ProductProperty) Indexes() []ent.Index {
	return []ent.Index{
		index.Fields("productKey", "identifier").Unique(),
	}
}

// Annotations of the ProductProperty
func (ProductProperty) Annotations() []schema.Annotation {
	return []schema.Annotation{
		entsql.Annotation{Table: "equip_product_properties"},
	}
}
