package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/dialect/entsql"
	"entgo.io/ent/schema"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
	"entgo.io/ent/schema/index"
	"github.com/suyuan32/simple-admin-common/orm/ent/mixins"
)

// ProductEvent holds the schema definition for the ProductEvent entity.
type ProductEvent struct {
	ent.Schema
}

// Fields of the ProductEvent.
func (ProductEvent) Fields() []ent.Field {
	return []ent.Field{
		field.String("productKey").
			Comment("Product Key | 产品Key").
			Annotations(entsql.WithComments(true)),
		field.Int64("createTs").
			Optional().
			Comment("CreateTs | 功能创建的时间戳").
			Annotations(entsql.WithComments(true)),
		field.String("version").
			Optional().
			Comment("Version | 物模型版本号").
			Annotations(entsql.WithComments(true)),
		field.String("identifier").
			Comment("Identifier | 事件的标识符").
			Annotations(entsql.WithComments(true)),
		field.String("eventName").
			Comment("EventName | 事件名称").
			Annotations(entsql.WithComments(true)),
		field.String("eventType").
			Optional().
			Default("INFO_EVENT_TYPE").
			Comment("EventType | 事件类型,INFO_EVENT_TYPE：信息。ALERT_EVENT_TYPE：告警。ERROR_EVENT_TYPE：故障。").
			Annotations(entsql.WithComments(true)),
		field.Text("outputData").
			Optional().
			Comment("Output data | 事件的输出参数").
			Annotations(entsql.WithComments(true)),
		field.Bool("custom").
			Default(true).
			Comment("Custom | 是否是自定义功能").
			Annotations(entsql.WithComments(true)),
		field.Bool("required").
			Default(false).
			Comment("Required | 是否是标准品类的必选事件").
			Annotations(entsql.WithComments(true)),
	}
}

// Edges of the ProductEvent.
func (ProductEvent) Edges() []ent.Edge {
	return []ent.Edge{
		edge.From("product", Product.Type).Ref("events").Unique(),
	}
}

// Mixin of the ProductEvent.
func (ProductEvent) Mixin() []ent.Mixin {
	return []ent.Mixin{
		mixins.IDMixin{},
		mixins.StatusMixin{},
		mixins.SortMixin{},
	}
}

// Indexes of the ProductEvent.
func (ProductEvent) Indexes() []ent.Index {
	return []ent.Index{
		index.Fields("productKey", "identifier").Unique(),
	}
}

// Annotations of the ProductEvent
func (ProductEvent) Annotations() []schema.Annotation {
	return []schema.Annotation{
		entsql.Annotation{Table: "equip_product_events"},
	}
}
