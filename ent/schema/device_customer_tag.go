package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/dialect/entsql"
	"entgo.io/ent/schema"
	"entgo.io/ent/schema/field"
	"entgo.io/ent/schema/index"
	"github.com/suyuan32/simple-admin-common/orm/ent/mixins"
)

// DeviceCustomerTag holds the schema definition for the DeviceCustomerTag entity.
type DeviceCustomerTag struct {
	ent.Schema
}

// Fields of the DeviceCustomerTag.
func (DeviceCustomerTag) Fields() []ent.Field {
	return []ent.Field{
		field.String("iotID").
			Comment("Iot Id | 设备ID").
			Annotations(entsql.WithComments(true)),
		field.String("deviceName").
			Comment("Device Name | 设备名称").
			Annotations(entsql.WithComments(true)),
		field.String("sn").
			Comment("SN_No | SN_No").
			Annotations(entsql.WithComments(true)),
		field.String("customer").
			Comment("Customer_E | 标签信息").
			Annotations(entsql.WithComments(true)),
		field.String("city").
			Comment("City | 城市").
			Annotations(entsql.WithComments(true)),
		field.String("location").
			Optional().
			Comment("Location | 地理位置").
			Annotations(entsql.WithComments(true)),
		field.String("line1").
			Comment("Line1 | 第一行封装信息").
			Annotations(entsql.WithComments(true)),
		field.String("line2").
			Comment("Line2 | 第二行封装信息").
			Annotations(entsql.WithComments(true)),
	}
}

// Edges of the DeviceCustomerTag.
func (DeviceCustomerTag) Edges() []ent.Edge {
	return []ent.Edge{}
}

// Mixin of the DeviceCustomerTag.
func (DeviceCustomerTag) Mixin() []ent.Mixin {
	return []ent.Mixin{
		mixins.IDMixin{},
		mixins.StatusMixin{},
		mixins.SortMixin{},
	}
}

// Indexes of the DeviceCustomerTag.
func (DeviceCustomerTag) Indexes() []ent.Index {
	return []ent.Index{
		index.Fields("iotID", "sn").Unique(),
	}
}

// Annotations of the DeviceCustomerTag
func (DeviceCustomerTag) Annotations() []schema.Annotation {
	return []schema.Annotation{
		entsql.Annotation{Table: "equip_device_customer_tags"},
	}
}
