package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/dialect/entsql"
	"entgo.io/ent/schema"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
	"entgo.io/ent/schema/index"
	"github.com/suyuan32/simple-admin-common/orm/ent/mixins"
)

// ProductService holds the schema definition for the ProductService entity.
type ProductService struct {
	ent.Schema
}

// Fields of the ProductService.
func (ProductService) Fields() []ent.Field {
	return []ent.Field{
		field.String("productKey").
			Comment("Product Key | 产品Key").
			Annotations(entsql.WithComments(true)),
		field.Int64("createTs").
			Optional().
			Comment("CreateTs | 功能创建的时间戳").
			Annotations(entsql.WithComments(true)),
		field.String("version").
			Optional().
			Comment("Version | 物模型版本号").
			Annotations(entsql.WithComments(true)),
		field.String("identifier").
			Comment("Identifier | 服务的标识符").
			Annotations(entsql.WithComments(true)),
		field.String("serviceName").
			Comment("ServiceName | 服务名称").
			Annotations(entsql.WithComments(true)),
		field.Text("inputParams").
			Optional().
			Comment("InputParams | 服务的输入参数").
			Annotations(entsql.WithComments(true)),
		field.Text("outputParams").
			Optional().
			Comment("OutputParams | 服务的输出参数").
			Annotations(entsql.WithComments(true)),
		field.Bool("required").
			Default(false).
			Comment("Required | 是否是标准品类的必选事件").
			Annotations(entsql.WithComments(true)),
		field.String("callType").
			Default("ASYNC").
			Comment("CallType | 服务的调用方式").
			Annotations(entsql.WithComments(true)),
		field.Bool("custom").
			Default(true).
			Comment("Custom | 是否是自定义功能").
			Annotations(entsql.WithComments(true)),
	}
}

// Edges of the ProductService.
func (ProductService) Edges() []ent.Edge {
	return []ent.Edge{
		edge.From("product", Product.Type).Ref("services").Unique(),
	}
}

// Mixin of the ProductService.
func (ProductService) Mixin() []ent.Mixin {
	return []ent.Mixin{
		mixins.IDMixin{},
		mixins.StatusMixin{},
		mixins.SortMixin{},
	}
}

// Indexes of the ProductService.
func (ProductService) Indexes() []ent.Index {
	return []ent.Index{
		index.Fields("productKey", "identifier").Unique(),
	}
}

// Annotations of the ProductService
func (ProductService) Annotations() []schema.Annotation {
	return []schema.Annotation{
		entsql.Annotation{Table: "equip_product_services"},
	}
}
