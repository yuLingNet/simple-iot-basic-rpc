package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/dialect/entsql"
	"entgo.io/ent/schema"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
	"entgo.io/ent/schema/index"
	"github.com/suyuan32/simple-admin-common/orm/ent/mixins"
)

// ProductTag holds the schema definition for the ProductTag entity.
type ProductTag struct {
	ent.Schema
}

// Fields of the ProductTag.
func (ProductTag) Fields() []ent.Field {
	return []ent.Field{
		field.String("productKey").
			Comment("Product Key | 产品Key").
			Annotations(entsql.WithComments(true)),
		field.String("tagKey").
			Comment("Tag Key | 标签键").
			Annotations(entsql.WithComments(true)),
		field.String("tagValue").
			Comment("Tag Value | 标签值").
			Annotations(entsql.WithComments(true)),
	}
}

// Edges of the ProductTag.
func (ProductTag) Edges() []ent.Edge {
	return []ent.Edge{
		edge.From("product", Product.Type).Ref("tags").Unique(),
	}
}

// Mixin of the ProductTag.
func (ProductTag) Mixin() []ent.Mixin {
	return []ent.Mixin{
		mixins.IDMixin{},
		mixins.StatusMixin{},
		mixins.SortMixin{},
	}
}

// Indexes of the ProductTag.
func (ProductTag) Indexes() []ent.Index {
	return []ent.Index{
		index.Fields("productKey", "tagKey").Unique(),
	}
}

// Annotations of the ProductTag
func (ProductTag) Annotations() []schema.Annotation {
	return []schema.Annotation{
		entsql.Annotation{Table: "equip_product_tags"},
	}
}
