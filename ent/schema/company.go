package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/dialect/entsql"
	"entgo.io/ent/schema"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
	"github.com/suyuan32/simple-admin-common/orm/ent/mixins"
)

// Company holds the schema definition for the Company entity.
type Company struct {
	ent.Schema
}

// Fields of the Company.
func (Company) Fields() []ent.Field {
	return []ent.Field{
		field.Int32("code").
			Comment("Code | 公司编码").
			Annotations(entsql.WithComments(true)),
		field.String("nickname").
			Comment("Nickname | 公司简称").
			Annotations(entsql.WithComments(true)),
		field.String("address").
			Optional().
			Comment("Address | 公司地址").
			Annotations(entsql.WithComments(true)),
		field.String("remark").
			Optional().
			Comment("Remark | 备注").
			Annotations(entsql.WithComments(true)),
	}
}

// Edges of the Company.
func (Company) Edges() []ent.Edge {
	return []ent.Edge{
		edge.To("devices", Device.Type),
		edge.To("members", Member.Type),
	}
}

// Mixin of the Company.
func (Company) Mixin() []ent.Mixin {
	return []ent.Mixin{
		mixins.IDMixin{},
		mixins.StatusMixin{},
		mixins.SortMixin{},
	}
}

// Indexes of the Company.
func (Company) Indexes() []ent.Index {
	return nil
}

// Annotations of the Company
func (Company) Annotations() []schema.Annotation {
	return []schema.Annotation{
		entsql.Annotation{Table: "client_companies"},
	}
}
