// Code generated by ent, DO NOT EDIT.

package producttag

import (
	"time"

	"entgo.io/ent/dialect/sql"
	"entgo.io/ent/dialect/sql/sqlgraph"
)

const (
	// Label holds the string label denoting the producttag type in the database.
	Label = "product_tag"
	// FieldID holds the string denoting the id field in the database.
	FieldID = "id"
	// FieldCreatedAt holds the string denoting the created_at field in the database.
	FieldCreatedAt = "created_at"
	// FieldUpdatedAt holds the string denoting the updated_at field in the database.
	FieldUpdatedAt = "updated_at"
	// FieldStatus holds the string denoting the status field in the database.
	FieldStatus = "status"
	// FieldSort holds the string denoting the sort field in the database.
	FieldSort = "sort"
	// FieldProductKey holds the string denoting the productkey field in the database.
	FieldProductKey = "product_key"
	// FieldTagKey holds the string denoting the tagkey field in the database.
	FieldTagKey = "tag_key"
	// FieldTagValue holds the string denoting the tagvalue field in the database.
	FieldTagValue = "tag_value"
	// EdgeProduct holds the string denoting the product edge name in mutations.
	EdgeProduct = "product"
	// Table holds the table name of the producttag in the database.
	Table = "equip_product_tags"
	// ProductTable is the table that holds the product relation/edge.
	ProductTable = "equip_product_tags"
	// ProductInverseTable is the table name for the Product entity.
	// It exists in this package in order to avoid circular dependency with the "product" package.
	ProductInverseTable = "equip_products"
	// ProductColumn is the table column denoting the product relation/edge.
	ProductColumn = "product_tags"
)

// Columns holds all SQL columns for producttag fields.
var Columns = []string{
	FieldID,
	FieldCreatedAt,
	FieldUpdatedAt,
	FieldStatus,
	FieldSort,
	FieldProductKey,
	FieldTagKey,
	FieldTagValue,
}

// ForeignKeys holds the SQL foreign-keys that are owned by the "equip_product_tags"
// table and are not defined as standalone fields in the schema.
var ForeignKeys = []string{
	"product_tags",
}

// ValidColumn reports if the column name is valid (part of the table columns).
func ValidColumn(column string) bool {
	for i := range Columns {
		if column == Columns[i] {
			return true
		}
	}
	for i := range ForeignKeys {
		if column == ForeignKeys[i] {
			return true
		}
	}
	return false
}

var (
	// DefaultCreatedAt holds the default value on creation for the "created_at" field.
	DefaultCreatedAt func() time.Time
	// DefaultUpdatedAt holds the default value on creation for the "updated_at" field.
	DefaultUpdatedAt func() time.Time
	// UpdateDefaultUpdatedAt holds the default value on update for the "updated_at" field.
	UpdateDefaultUpdatedAt func() time.Time
	// DefaultStatus holds the default value on creation for the "status" field.
	DefaultStatus uint8
	// DefaultSort holds the default value on creation for the "sort" field.
	DefaultSort uint32
)

// OrderOption defines the ordering options for the ProductTag queries.
type OrderOption func(*sql.Selector)

// ByID orders the results by the id field.
func ByID(opts ...sql.OrderTermOption) OrderOption {
	return sql.OrderByField(FieldID, opts...).ToFunc()
}

// ByCreatedAt orders the results by the created_at field.
func ByCreatedAt(opts ...sql.OrderTermOption) OrderOption {
	return sql.OrderByField(FieldCreatedAt, opts...).ToFunc()
}

// ByUpdatedAt orders the results by the updated_at field.
func ByUpdatedAt(opts ...sql.OrderTermOption) OrderOption {
	return sql.OrderByField(FieldUpdatedAt, opts...).ToFunc()
}

// ByStatus orders the results by the status field.
func ByStatus(opts ...sql.OrderTermOption) OrderOption {
	return sql.OrderByField(FieldStatus, opts...).ToFunc()
}

// BySort orders the results by the sort field.
func BySort(opts ...sql.OrderTermOption) OrderOption {
	return sql.OrderByField(FieldSort, opts...).ToFunc()
}

// ByProductKey orders the results by the productKey field.
func ByProductKey(opts ...sql.OrderTermOption) OrderOption {
	return sql.OrderByField(FieldProductKey, opts...).ToFunc()
}

// ByTagKey orders the results by the tagKey field.
func ByTagKey(opts ...sql.OrderTermOption) OrderOption {
	return sql.OrderByField(FieldTagKey, opts...).ToFunc()
}

// ByTagValue orders the results by the tagValue field.
func ByTagValue(opts ...sql.OrderTermOption) OrderOption {
	return sql.OrderByField(FieldTagValue, opts...).ToFunc()
}

// ByProductField orders the results by product field.
func ByProductField(field string, opts ...sql.OrderTermOption) OrderOption {
	return func(s *sql.Selector) {
		sqlgraph.OrderByNeighborTerms(s, newProductStep(), sql.OrderByField(field, opts...))
	}
}
func newProductStep() *sqlgraph.Step {
	return sqlgraph.NewStep(
		sqlgraph.From(Table, FieldID),
		sqlgraph.To(ProductInverseTable, FieldID),
		sqlgraph.Edge(sqlgraph.M2O, true, ProductTable, ProductColumn),
	)
}
