package main

import (
	"context"
	"database/sql"
	"entgo.io/ent/dialect/sql/schema"
	"fmt"
	basicEnt "gitee.com/yuLingNet/simple-iot-basic-rpc/ent"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/ent/device"
	dataEnt "gitee.com/yuLingNet/simple-iot-data-rpc/ent"
	"gitee.com/yuLingNet/simple-iot-data-rpc/ent/deviceevent"
	"gitee.com/yuLingNet/simple-iot-data-rpc/ent/deviceupstercycle"
	_ "github.com/go-sql-driver/mysql"
	_ "github.com/jinzhu/gorm/dialects/mssql"
	"log"
	"os"
	"strconv"
	"strings"
	"time"
)

/**
*查询旧表数据 upster statistics
 */
func statistics(productKey string, year int, db *sql.DB, count int, client *dataEnt.Client) {
	type UpsterCycle struct {
		id         int
		iotId      string
		deviceName string
		productKey string
		dateTime   string
		eventTime  string
		Num1       int32
		Num2       int32
		Num3       int32
		NumCycle   int32
		hours      string
		init       bool
		createdAt  time.Time
		updatedAt  time.Time
	}

	sql := "SELECT count(*) FROM " + productKey + "_upster_statistical_" + strconv.Itoa(year)

	res, err := db.Query(sql)
	if err != nil {
		log.Println(err.Error())
		return
	}
	for res.Next() {
		_ = res.Scan(&count)
	}
	fmt.Println("count:", count)

	if count > 0 {
		page := 1
		for {
			sql := "SELECT * FROM " + productKey + "_upster_statistical_" + strconv.Itoa(year) + " LIMIT 3000 OFFSET " + strconv.Itoa((page-1)*3000)
			rows, err := db.Query(sql)
			if err != nil {
				log.Panicln(err)
			}
			var bulk []*dataEnt.DeviceUpsterStatisticsCreate
			// 遍历查询结果
			for rows.Next() {
				var cycle UpsterCycle
				if err := rows.Scan(&cycle.id, &cycle.iotId, &cycle.deviceName, &cycle.productKey, &cycle.dateTime, &cycle.eventTime, &cycle.Num1, &cycle.Num2, &cycle.Num3, &cycle.NumCycle, &cycle.hours, &cycle.init, &cycle.createdAt, &cycle.updatedAt); err != nil {
					log.Fatal(err)
				}
				bulk = append(bulk, client.DeviceUpsterStatistics.Create().
					//SetID(uint64(cycle.id)).
					SetNotNilIotID(&cycle.iotId).
					SetProductKey(cycle.productKey).
					SetDeviceName(cycle.deviceName).
					SetDate(cycle.dateTime).
					SetTimestamp(cycle.eventTime).
					SetNumber1(cycle.Num1).
					SetNumber2(cycle.Num2).
					SetNumber3(cycle.Num3).
					SetNumberCycles(cycle.NumCycle).
					SetHours(cycle.hours).
					SetInitialize(cycle.init).
					SetCreatedAt(cycle.createdAt).
					SetUpdatedAt(cycle.updatedAt))
			}
			err = client.DeviceUpsterStatistics.CreateBulk(bulk...).
				OnConflict().
				UpdateNewValues().
				Exec(context.Background())
			if err != nil {
				log.Fatal(err)
			}
			log.Println("table:", productKey+"_upster_statistical_"+strconv.Itoa(year), "data created to:DeviceUpsterStatistics, num:", page*3000)
			page++
			if page > (count/3000)+1 {
				break
			}
		}
	}
}

/**
*查询旧表数据 事件表
 */
func event(productKey string, iotId string, year int, db *sql.DB, count int, client *dataEnt.Client, timestamp int) {

	//自动迁移start
	err := client.Schema.Create(
		context.Background(),
		schema.WithHooks(func(next schema.Creator) schema.Creator {
			return schema.CreateFunc(func(ctx context.Context, tables ...*schema.Table) error {
				var dynamicTables *schema.Table
				for _, v := range tables {
					//按年份分表
					if strings.Contains(v.Name, deviceevent.Table) {
						nv := v
						nv.Name = deviceevent.Table + "_" + iotId + "_" + strconv.Itoa(year)
						dynamicTables = nv
					}
				}
				return next.Create(ctx, dynamicTables)
			})
		}),
	)
	if err != nil {
		if !strings.Contains(err.Error(), "already exists") {
			log.Fatal(err)
		}
	}
	//自动迁移end

	type Event struct {
		id         int
		iotId      string
		deviceName string
		productKey string
		version    string
		identifier string
		name       string
		eventType  string
		time       string
		outputData string
		createdAt  time.Time
		updatedAt  time.Time
	}

	sql := "SELECT count(*) FROM " + productKey + "_events_" + strconv.Itoa(year) + " WHERE `iotId` = \"" + iotId + "\" AND `time` > " + strconv.Itoa(timestamp)

	res, err := db.Query(sql)
	if err != nil {
		log.Println(err.Error())
		return
	}
	for res.Next() {
		_ = res.Scan(&count)
	}
	fmt.Println("count:", count)

	if count > 0 {
		page := 1
		for {
			sql := "SELECT * FROM " + productKey + "_events_" + strconv.Itoa(year) + " WHERE `iotId` = \"" + iotId + "\" AND `time` > " + strconv.Itoa(timestamp) + " LIMIT 3000 OFFSET " + strconv.Itoa((page-1)*3000)
			rows, err := db.Query(sql)
			if err != nil {
				log.Panicln(err)
			}
			var bulk []*dataEnt.DeviceEventCreate
			// 遍历查询结果
			for rows.Next() {
				var event Event
				if err := rows.Scan(&event.id, &event.iotId, &event.deviceName, &event.productKey, &event.version, &event.identifier, &event.name, &event.eventType, &event.time, &event.outputData, &event.createdAt, &event.updatedAt); err != nil {
					log.Fatal(err)
				}
				bulk = append(bulk, client.DeviceEvent.Create().
					TableName(deviceevent.Table+"_"+iotId+"_"+strconv.Itoa(year)).
					SetNotNilIotID(&event.iotId).
					SetNotNilEventType(&event.eventType).
					SetNotNilIdentifier(&event.identifier).
					SetNotNilName(&event.name).
					SetNotNilOutputData(&event.outputData).
					SetNotNilTime(&event.time).
					SetCreatedAt(event.createdAt).
					SetUpdatedAt(event.updatedAt))
			}
			err = client.DeviceEvent.CreateBulk(bulk...).
				OnConflict().
				Ignore().
				Exec(context.Background())
			if err != nil {
				log.Fatal(err)
			}
			log.Println("table:", productKey+"_events_"+strconv.Itoa(year), "data created to:", deviceevent.Table+"_"+iotId+"_"+strconv.Itoa(year), "num:", page*3000)
			page++
			if page > (count/3000)+1 {
				break
			}
		}
	}
}

/**
*查询旧表数据 upster cycle log
 */
func wash_log(productKey string, year int, db *sql.DB, count int, client *dataEnt.Client) {
	//自动迁移start
	err := client.Schema.Create(
		context.Background(),
		schema.WithHooks(func(next schema.Creator) schema.Creator {
			return schema.CreateFunc(func(ctx context.Context, tables ...*schema.Table) error {
				var dynamicTables *schema.Table
				for _, v := range tables {
					//按年份分表
					if strings.Contains(v.Name, deviceupstercycle.Table) {
						nv := v
						nv.Name = deviceupstercycle.Table + "_" + productKey + "_" + strconv.Itoa(year)
						dynamicTables = nv
					}
				}
				return next.Create(ctx, dynamicTables)
			})
		}),
	)
	if err != nil {
		if !strings.Contains(err.Error(), "already exists") {
			log.Fatal(err)
		}
	}
	//自动迁移end

	type UpsterCycle struct {
		id         int
		iotId      string
		productKey string
		deviceName string
		dateTime   string
		eventTime  string
		eventDate  string
		Num1       int32
		Num2       int32
		Num3       int32
		NumRegen   int32
		NumRinsing int32
		NumCycle   int32
		TotalTime  int32
		createdAt  time.Time
		updatedAt  time.Time
	}

	sql := "SELECT count(*) FROM " + productKey + "_wash_cycles_log_" + strconv.Itoa(year)

	res, err := db.Query(sql)
	if err != nil {
		log.Println(err.Error())
		return
	}
	for res.Next() {
		_ = res.Scan(&count)
	}
	fmt.Println("count:", count)

	if count > 0 {
		page := 1
		for {
			sql := "SELECT * FROM " + productKey + "_wash_cycles_log_" + strconv.Itoa(year) + " LIMIT 3000 OFFSET " + strconv.Itoa((page-1)*3000)
			rows, err := db.Query(sql)
			if err != nil {
				log.Panicln(err)
			}
			var bulk []*dataEnt.DeviceUpsterCycleCreate
			// 遍历查询结果
			for rows.Next() {
				var cycle UpsterCycle
				if err := rows.Scan(&cycle.id, &cycle.iotId, &cycle.productKey, &cycle.deviceName, &cycle.dateTime, &cycle.eventTime, &cycle.eventDate, &cycle.Num1, &cycle.Num2, &cycle.Num3, &cycle.NumRegen, &cycle.NumRinsing, &cycle.NumCycle, &cycle.TotalTime, &cycle.createdAt, &cycle.updatedAt); err != nil {
					log.Fatal(err)
				}
				bulk = append(bulk, client.DeviceUpsterCycle.Create().
					TableName(deviceupstercycle.Table+"_"+productKey+"_"+strconv.Itoa(year)).
					//SetID(uint64(cycle.id)).
					SetNotNilIotID(&cycle.iotId).
					SetNotNilProductKey(&cycle.productKey).
					SetNotNilDeviceName(&cycle.deviceName).
					SetNotNilTimestamp(&cycle.eventTime).
					SetNotNilNumber1(&cycle.Num1).
					SetNotNilNumber2(&cycle.Num2).
					SetNotNilNumber3(&cycle.Num3).
					SetNotNilNumberCycles(&cycle.NumCycle).
					SetNotNilNumberRegeneration(&cycle.NumRegen).
					SetNotNilNumberRinsingCycles(&cycle.NumRinsing).
					SetNotNilTotalOperationTime(&cycle.TotalTime).
					SetCreatedAt(cycle.createdAt).
					SetUpdatedAt(cycle.updatedAt))
			}
			err = client.DeviceUpsterCycle.CreateBulk(bulk...).
				OnConflict().
				Ignore().
				Exec(context.Background())
			if err != nil {
				log.Fatal(err)
			}
			log.Println("table:", productKey+"_wash_cycles_log_"+strconv.Itoa(year), "data created to:", deviceupstercycle.Table+"_"+productKey+"_"+strconv.Itoa(year), "num:", page*3000)
			page++
			if page > (count/3000)+1 {
				break
			}
		}
	}
}

func main() {
	/**
	*1.构建链接旧数据库
	 */
	// 构建连接串
	dsn := "meikodev:Meiko!666666@tcp(rm-uf6olz8yz01y11jwxlo.mysql.rds.aliyuncs.com:3306)/meiko-stable?parseTime=True"

	// 连接数据库
	db, err := sql.Open("mysql", dsn)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	// 检查数据库连接是否成功
	if err := db.Ping(); err != nil {
		log.Fatal(err)
	}

	/**
	*2.构建链接新数据库
	 */
	clientData, err := dataEnt.Open("mysql", "meikodev:Meiko!666666@tcp(rm-uf6olz8yz01y11jwxlo.mysql.rds.aliyuncs.com:3306)/msa_meiko?parseTime=True&charset=utf8mb4&loc=Asia%2FShanghai")
	if err != nil {
		log.Fatalf("failed opening connection to mysql: %v", err)
	}
	defer clientData.Close()

	clientBasic, err := basicEnt.Open("mysql", "meikodev:Meiko!666666@tcp(rm-uf6olz8yz01y11jwxlo.mysql.rds.aliyuncs.com:3306)/msa_meiko?parseTime=True&charset=utf8mb4&loc=Asia%2FShanghai")
	if err != nil {
		log.Fatalf("failed opening connection to mysql: %v", err)
	}
	defer clientBasic.Close()

	/**
	*定义参数
	 */
	var count int

	//productKeys := []string{"ge6mBKXg0tp"}
	productKeys := []string{"ge6mBKXg0tp", "a1YjGbjd7Sf", "a14Ky5AZEFb", "a1OnSzuQ70X"}

	// 检查是否有足够的参数传入
	if len(os.Args) < 2 {
		fmt.Println("请输入一个变量作为参数")
		return
	}
	// 获取第二个参数（第一个参数是程序本身）
	date := os.Args[1]
	loc, _ := time.LoadLocation("Asia/Shanghai")
	dateTime, _ := time.ParseInLocation("2006-01-02", date, loc)
	timestamp := dateTime.UnixMilli()

	years := []int{2024}
	for _, productKey := range productKeys {
		fmt.Println(productKey)
		for _, year := range years {
			devices, err := clientBasic.Device.Query().Where(device.ProductKeyEQ(productKey)).All(context.Background())
			if err != nil {
				log.Fatal(err)
			}
			for i, device := range devices {
				fmt.Println(i+1, ":", device.DeviceName, "-", device.IotID)
				event(productKey, device.IotID, year, db, count, clientData, int(timestamp))
			}
			wash_log(productKey, year, db, count, clientData)
			statistics(productKey, year, db, count, clientData)
		}
	}
	return

}
