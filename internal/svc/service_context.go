package svc

import (
	"gitee.com/yuLingNet/simple-iot-basic-rpc/ent"
	_ "gitee.com/yuLingNet/simple-iot-basic-rpc/ent/runtime"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/config"
	coreEnt "github.com/suyuan32/simple-admin-core/rpc/ent"

	"github.com/redis/go-redis/v9"
	"github.com/zeromicro/go-zero/core/logx"
)

type ServiceContext struct {
	Config config.Config
	DB     *ent.Client
	DBCore *coreEnt.Client
	Redis  redis.UniversalClient
}

func NewServiceContext(c config.Config) *ServiceContext {
	db := ent.NewClient(
		ent.Log(logx.Info), // logger
		ent.Driver(c.DatabaseConf.NewNoCacheDriver()),
		ent.Debug(), // debug mode
	)

	coreDb := coreEnt.NewClient(
		coreEnt.Log(logx.Info), // logger
		coreEnt.Driver(c.DatabaseConf.NewNoCacheDriver()),
		coreEnt.Debug(), // debug mode
	)

	return &ServiceContext{
		Config: c,
		DB:     db,
		DBCore: coreDb,
		Redis:  c.RedisConf.MustNewUniversalRedis(),
	}
}
