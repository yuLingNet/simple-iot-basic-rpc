package product

import (
	"context"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/ent"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/ent/predicate"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/ent/product"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/svc"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/utils/dberrorhandler"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/types/basic"
	"strings"

	"github.com/suyuan32/simple-admin-common/utils/pointy"
	"github.com/zeromicro/go-zero/core/logx"
)

type GetProductListLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGetProductListLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetProductListLogic {
	return &GetProductListLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *GetProductListLogic) GetProductList(in *basic.ProductListReq) (*basic.ProductListResp, error) {
	var predicates []predicate.Product
	if in.ProductKey != nil {
		predicates = append(predicates, product.ProductKeyContains(*in.ProductKey))
	}
	if in.ProductName != nil {
		predicates = append(predicates, product.ProductNameContains(*in.ProductName))
	}
	if in.Group != nil {
		switch *in.Group {
		case "upster":
			predicates = append(predicates, product.ProductKeyIn(l.svcCtx.Config.UpsterProductKeys...))
		case "mer-mef":
			productKeys := append(l.svcCtx.Config.MerProductKeys, l.svcCtx.Config.MefProductKeys...)
			predicates = append(predicates, product.ProductKeyIn(productKeys...))
		case "elephant":
			predicates = append(predicates, product.ProductKeyIn(l.svcCtx.Config.ElephantProductKeys...))

		}
	}
	result, err := l.svcCtx.DB.Product.Query().Where(predicates...).Page(l.ctx, in.Page, in.PageSize, func(pager *ent.ProductPager) {
		if in.Prop != nil && in.Order != nil {
			if *in.Order == "descending" {
				pager.Order = ent.Desc(camelCaseToUnderscore(*in.Prop))
			} else {
				pager.Order = ent.Asc(camelCaseToUnderscore(*in.Prop))
			}
		}
	})

	if err != nil {
		return nil, dberrorhandler.DefaultEntError(l.Logger, err, in)
	}

	resp := &basic.ProductListResp{}
	resp.Total = result.PageDetails.Total

	for _, v := range result.List {
		resp.Data = append(resp.Data, &basic.ProductInfo{
			Id:                  pointy.GetPointer(v.ID.String()),
			CreatedAt:           pointy.GetPointer(v.CreatedAt.UnixMilli()),
			UpdatedAt:           pointy.GetPointer(v.UpdatedAt.UnixMilli()),
			Status:              pointy.GetPointer(uint32(v.Status)),
			Sort:                &v.Sort,
			ProductKey:          &v.ProductKey,
			ProductName:         &v.ProductName,
			AuthType:            &v.AuthType,
			DataFormat:          &v.DataFormat,
			Description:         &v.Description,
			DeviceCount:         &v.DeviceCount,
			GmtCreate:           &v.GmtCreate,
			NodeType:            &v.NodeType,
			AliyunCommodityCode: &v.AliyunCommodityCode,
			CategoryKey:         &v.CategoryKey,
			CategoryName:        &v.CategoryName,
			Id2:                 &v.Id2,
			NetType:             &v.NetType,
			Owner:               &v.Owner,
			ProductSecret:       &v.ProductSecret,
			ProductStatus:       &v.ProductStatus,
			ProtocolType:        &v.ProtocolType,
			ValidateType:        &v.ValidateType,
		})
	}

	return resp, nil
}

func camelCaseToUnderscore(s string) string {
	data := make([]byte, 0, len(s)*2)
	j := false
	num := len(s)
	for i := 0; i < num; i++ {
		d := s[i]
		// or通过ASCII码进行大小写的转化
		// 65-90（A-Z），97-122（a-z）
		//判断如果字母为大写的A-Z就在前面拼接一个_
		if i > 0 && d >= 'A' && d <= 'Z' && j {
			data = append(data, '_')
		}
		if d != '_' {
			j = true
		}
		data = append(data, d)
	}
	//ToLower把大写字母统一转小写
	return strings.ToLower(string(data[:]))
}
