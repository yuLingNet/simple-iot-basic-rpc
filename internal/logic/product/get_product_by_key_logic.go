package product

import (
	"context"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/ent/product"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/svc"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/utils/dberrorhandler"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/types/basic"
	"github.com/suyuan32/simple-admin-common/utils/pointy"

	"github.com/zeromicro/go-zero/core/logx"
)

type GetProductByKeyLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGetProductByKeyLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetProductByKeyLogic {
	return &GetProductByKeyLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *GetProductByKeyLogic) GetProductByKey(in *basic.ProductKeyReq) (*basic.ProductInfo, error) {
	result, err := l.svcCtx.DB.Product.Query().Where(product.ProductKeyEQ(in.ProductKey)).First(l.ctx)
	if err != nil {
		return nil, dberrorhandler.DefaultEntError(l.Logger, err, in)
	}

	return &basic.ProductInfo{
		Id:                  pointy.GetPointer(result.ID.String()),
		CreatedAt:           pointy.GetPointer(result.CreatedAt.UnixMilli()),
		UpdatedAt:           pointy.GetPointer(result.UpdatedAt.UnixMilli()),
		Status:              pointy.GetPointer(uint32(result.Status)),
		Sort:                &result.Sort,
		ProductKey:          &result.ProductKey,
		ProductName:         &result.ProductName,
		AuthType:            &result.AuthType,
		DataFormat:          &result.DataFormat,
		Description:         &result.Description,
		DeviceCount:         &result.DeviceCount,
		GmtCreate:           &result.GmtCreate,
		NodeType:            &result.NodeType,
		AliyunCommodityCode: &result.AliyunCommodityCode,
		CategoryKey:         &result.CategoryKey,
		CategoryName:        &result.CategoryName,
		Id2:                 &result.Id2,
		NetType:             &result.NetType,
		Owner:               &result.Owner,
		ProductSecret:       &result.ProductSecret,
		ProductStatus:       &result.ProductStatus,
		ProtocolType:        &result.ProtocolType,
		ValidateType:        &result.ValidateType,
	}, nil
}
