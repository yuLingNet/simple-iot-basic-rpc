package product

import (
	"context"
	"github.com/suyuan32/simple-admin-common/i18n"

	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/svc"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/utils/dberrorhandler"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/types/basic"

	"github.com/suyuan32/simple-admin-common/utils/pointy"
	"github.com/suyuan32/simple-admin-common/utils/uuidx"
	"github.com/zeromicro/go-zero/core/logx"
)

type UpdateProductLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewUpdateProductLogic(ctx context.Context, svcCtx *svc.ServiceContext) *UpdateProductLogic {
	return &UpdateProductLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *UpdateProductLogic) UpdateProduct(in *basic.ProductInfo) (*basic.BaseResp, error) {
	query := l.svcCtx.DB.Product.UpdateOneID(uuidx.ParseUUIDString(*in.Id)).
		SetNotNilSort(in.Sort).
		SetNotNilProductKey(in.ProductKey).
		SetNotNilProductName(in.ProductName).
		SetNotNilAuthType(in.AuthType).
		SetNotNilDataFormat(in.DataFormat).
		SetNotNilDescription(in.Description).
		SetNotNilDeviceCount(in.DeviceCount).
		SetNotNilGmtCreate(in.GmtCreate).
		SetNotNilNodeType(in.NodeType).
		SetNotNilAliyunCommodityCode(in.AliyunCommodityCode).
		SetNotNilCategoryKey(in.CategoryKey).
		SetNotNilCategoryName(in.CategoryName).
		SetNotNilId2(in.Id2).
		SetNotNilNetType(in.NetType).
		SetNotNilOwner(in.Owner).
		SetNotNilProductSecret(in.ProductSecret).
		SetNotNilProductStatus(in.ProductStatus).
		SetNotNilProtocolType(in.ProtocolType).
		SetNotNilValidateType(in.ValidateType)

	if in.Status != nil {
		query.SetNotNilStatus(pointy.GetPointer(uint8(*in.Status)))
	}

	err := query.Exec(l.ctx)

	if err != nil {
		return nil, dberrorhandler.DefaultEntError(l.Logger, err, in)
	}

	return &basic.BaseResp{Msg: i18n.UpdateSuccess}, nil
}
