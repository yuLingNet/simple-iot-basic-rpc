package product

import (
	"context"
	"github.com/suyuan32/simple-admin-common/i18n"

	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/svc"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/utils/dberrorhandler"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/types/basic"

	"github.com/suyuan32/simple-admin-common/utils/pointy"
	"github.com/zeromicro/go-zero/core/logx"
)

type CreateProductLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewCreateProductLogic(ctx context.Context, svcCtx *svc.ServiceContext) *CreateProductLogic {
	return &CreateProductLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *CreateProductLogic) CreateProduct(in *basic.ProductInfo) (*basic.BaseUUIDResp, error) {
	query := l.svcCtx.DB.Product.Create().
		SetNotNilSort(in.Sort).
		SetNotNilProductKey(in.ProductKey).
		SetNotNilProductName(in.ProductName).
		SetNotNilAuthType(in.AuthType).
		SetNotNilDataFormat(in.DataFormat).
		SetNotNilDescription(in.Description).
		SetNotNilDeviceCount(in.DeviceCount).
		SetNotNilGmtCreate(in.GmtCreate).
		SetNotNilNodeType(in.NodeType).
		SetNotNilAliyunCommodityCode(in.AliyunCommodityCode).
		SetNotNilCategoryKey(in.CategoryKey).
		SetNotNilCategoryName(in.CategoryName).
		SetNotNilId2(in.Id2).
		SetNotNilNetType(in.NetType).
		SetNotNilOwner(in.Owner).
		SetNotNilProductSecret(in.ProductSecret).
		SetNotNilProductStatus(in.ProductStatus).
		SetNotNilProtocolType(in.ProtocolType).
		SetNotNilValidateType(in.ValidateType)

	if in.Status != nil {
		query.SetNotNilStatus(pointy.GetPointer(uint8(*in.Status)))
	}

	result, err := query.Save(l.ctx)

	if err != nil {
		return nil, dberrorhandler.DefaultEntError(l.Logger, err, in)
	}

	return &basic.BaseUUIDResp{Id: result.ID.String(), Msg: i18n.CreateSuccess}, nil
}
