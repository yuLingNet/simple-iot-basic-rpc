package product

import (
	"context"
	"gitee.com/yuLingNet/simple-drive-aliyun/aliyun"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/ent"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/svc"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/types/basic"
	"github.com/suyuan32/simple-admin-common/i18n"

	"github.com/zeromicro/go-zero/core/logx"
)

type SyncProductLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewSyncProductLogic(ctx context.Context, svcCtx *svc.ServiceContext) *SyncProductLogic {
	return &SyncProductLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *SyncProductLogic) SyncProduct(in *basic.Empty) (*basic.BaseResp, error) {
	//调用阿里云api
	err := QueryProductList(1, 20, l)
	if err != nil {
		return nil, err
	}

	return &basic.BaseResp{Msg: i18n.Success}, nil
}

func QueryProductList(page int32, size int32, l *SyncProductLogic) error {
	productListResult, err := aliyun.QueryProductList(page, size)
	if err != nil {
		if err != nil {
			return err
		}
	}

	list := productListResult.Data.List.ProductInfo
	if len(list) > 0 {
		bulk := make([]*ent.ProductCreate, len(list))
		for i, item := range list {
			productResult, err := aliyun.QueryProduct(*item.ProductKey)
			if err != nil {
				return err
			}
			detail := productResult.Data

			bulk[i] = l.svcCtx.DB.Product.Create().
				SetNotNilProductKey(item.ProductKey).
				SetNotNilProductName(item.ProductName).
				SetNotNilAuthType(item.AuthType).
				SetNotNilDataFormat(item.DataFormat).
				SetNotNilDescription(item.Description).
				SetNotNilDeviceCount(item.DeviceCount).
				SetNotNilGmtCreate(item.GmtCreate).
				SetNotNilNodeType(item.NodeType).
				SetNotNilAliyunCommodityCode(detail.AliyunCommodityCode).
				SetNotNilCategoryKey(detail.CategoryKey).
				SetNotNilCategoryName(detail.CategoryName).
				SetNotNilId2(detail.Id2).
				SetNotNilNetType(detail.NetType).
				SetNotNilOwner(detail.Owner).
				SetNotNilProductSecret(detail.ProductSecret).
				SetNotNilProductStatus(detail.ProductStatus).
				SetNotNilProtocolType(detail.ProtocolType).
				SetNotNilValidateType(detail.ValidateType)
		}
		err := l.svcCtx.DB.Product.CreateBulk(bulk...).
			OnConflict().
			UpdateNewValues().
			Exec(l.ctx)
		if err != nil {
			return err
		}
	}

	if *productListResult.Data.PageCount > 1 && page < *productListResult.Data.PageCount {
		page++
		err := QueryProductList(page, size, l)
		if err != nil {
			return err
		}
	}
	return nil
}
