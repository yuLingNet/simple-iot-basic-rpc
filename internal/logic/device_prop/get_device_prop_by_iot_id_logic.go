package device_prop

import (
	"context"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/ent/deviceprop"
	"github.com/suyuan32/simple-admin-common/utils/pointy"

	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/svc"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/types/basic"

	"github.com/zeromicro/go-zero/core/logx"
)

type GetDevicePropByIotIdLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGetDevicePropByIotIdLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetDevicePropByIotIdLogic {
	return &GetDevicePropByIotIdLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *GetDevicePropByIotIdLogic) GetDevicePropByIotId(in *basic.DeviceIotIdReq) (*basic.DevicePropInfo, error) {
	result, err := l.svcCtx.DB.DeviceProp.Query().Where(deviceprop.IotIDEQ(in.IotId)).First(l.ctx)
	if err != nil {
		//return nil, dberrorhandler.DefaultEntError(l.Logger, err, in)
		return &basic.DevicePropInfo{}, nil
	}

	return &basic.DevicePropInfo{
		Id:        &result.ID,
		CreatedAt: pointy.GetPointer(result.CreatedAt.UnixMilli()),
		UpdatedAt: pointy.GetPointer(result.UpdatedAt.UnixMilli()),
		Status:    pointy.GetPointer(uint32(result.Status)),
		Sort:      &result.Sort,
		IotID:     &result.IotID,
		Props:     &result.Props,
	}, nil
}
