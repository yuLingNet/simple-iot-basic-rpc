package device_prop

import (
	"context"
	"gitee.com/yuLingNet/simple-drive-aliyun/aliyun"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/ent/device"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/svc"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/types/basic"
	"github.com/suyuan32/simple-admin-common/i18n"

	"github.com/zeromicro/go-zero/core/logx"
)

type SyncDevicePropLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewSyncDevicePropLogic(ctx context.Context, svcCtx *svc.ServiceContext) *SyncDevicePropLogic {
	return &SyncDevicePropLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *SyncDevicePropLogic) SyncDeviceProp(in *basic.Empty) (*basic.BaseResp, error) {
	//获取产品ProductKey
	iotIds := l.svcCtx.DB.Device.Query().Select("iot_id").StringsX(l.ctx)
	for _, iotId := range iotIds {
		//调用阿里云api
		err := QueryDeviceProp(iotId, l)
		if err != nil {
			return nil, err
		}
	}

	return &basic.BaseResp{Msg: i18n.Success}, nil
}

func QueryDeviceProp(iotId string, l *SyncDevicePropLogic) error {
	devicePropResult, err := aliyun.QueryDeviceProp(iotId)
	if err != nil {
		return err
	}

	props := devicePropResult.Props
	if props != nil {
		device, _ := l.svcCtx.DB.Device.Query().Where(device.IotIDEQ(iotId)).First(l.ctx)
		err = l.svcCtx.DB.DeviceProp.Create().
			SetNotNilIotID(&iotId).
			SetNotNilProps(props).
			SetDevice(device).
			OnConflict().
			UpdateNewValues().
			Exec(l.ctx)
		if err != nil {
			return err
		}
	}
	return nil
}
