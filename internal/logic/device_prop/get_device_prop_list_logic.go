package device_prop

import (
	"context"

	"gitee.com/yuLingNet/simple-iot-basic-rpc/ent/deviceprop"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/ent/predicate"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/svc"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/utils/dberrorhandler"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/types/basic"

	"github.com/suyuan32/simple-admin-common/utils/pointy"
    "github.com/zeromicro/go-zero/core/logx"
)

type GetDevicePropListLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGetDevicePropListLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetDevicePropListLogic {
	return &GetDevicePropListLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *GetDevicePropListLogic) GetDevicePropList(in *basic.DevicePropListReq) (*basic.DevicePropListResp, error) {
	var predicates []predicate.DeviceProp
	if in.IotID != nil {
		predicates = append(predicates, deviceprop.IotIDContains(*in.IotID))
	}
	if in.Props != nil {
		predicates = append(predicates, deviceprop.PropsContains(*in.Props))
	}
	result, err := l.svcCtx.DB.DeviceProp.Query().Where(predicates...).Page(l.ctx, in.Page, in.PageSize)

	if err != nil {
		return nil, dberrorhandler.DefaultEntError(l.Logger, err, in)
	}

	resp := &basic.DevicePropListResp{}
	resp.Total = result.PageDetails.Total

	for _, v := range result.List {
		resp.Data = append(resp.Data, &basic.DevicePropInfo{
			Id:          &v.ID,
			CreatedAt:   pointy.GetPointer(v.CreatedAt.UnixMilli()),
			UpdatedAt:   pointy.GetPointer(v.UpdatedAt.UnixMilli()),
			Status:	pointy.GetPointer(uint32(v.Status)),
			Sort:	&v.Sort,
			IotID:	&v.IotID,
			Props:	&v.Props,
		})
	}

	return resp, nil
}
