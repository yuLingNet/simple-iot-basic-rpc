package device_prop

import (
	"context"
	"github.com/suyuan32/simple-admin-common/i18n"

	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/svc"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/utils/dberrorhandler"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/types/basic"

	"github.com/suyuan32/simple-admin-common/utils/pointy"
	"github.com/zeromicro/go-zero/core/logx"
)

type UpdateDevicePropLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewUpdateDevicePropLogic(ctx context.Context, svcCtx *svc.ServiceContext) *UpdateDevicePropLogic {
	return &UpdateDevicePropLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *UpdateDevicePropLogic) UpdateDeviceProp(in *basic.DevicePropInfo) (*basic.BaseResp, error) {
	query := l.svcCtx.DB.DeviceProp.UpdateOneID(*in.Id).
		SetNotNilSort(in.Sort).
		SetNotNilIotID(in.IotID).
		SetNotNilProps(in.Props)

	if in.Status != nil {
		query.SetNotNilStatus(pointy.GetPointer(uint8(*in.Status)))
	}

	err := query.Exec(l.ctx)

	if err != nil {
		return nil, dberrorhandler.DefaultEntError(l.Logger, err, in)
	}

	return &basic.BaseResp{Msg: i18n.UpdateSuccess}, nil
}
