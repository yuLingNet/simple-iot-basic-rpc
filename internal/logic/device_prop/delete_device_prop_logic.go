package device_prop

import (
	"context"
	"github.com/suyuan32/simple-admin-common/i18n"

	"gitee.com/yuLingNet/simple-iot-basic-rpc/ent/deviceprop"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/svc"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/utils/dberrorhandler"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/types/basic"

	"github.com/zeromicro/go-zero/core/logx"
)

type DeleteDevicePropLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewDeleteDevicePropLogic(ctx context.Context, svcCtx *svc.ServiceContext) *DeleteDevicePropLogic {
	return &DeleteDevicePropLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *DeleteDevicePropLogic) DeleteDeviceProp(in *basic.IDsReq) (*basic.BaseResp, error) {
	_, err := l.svcCtx.DB.DeviceProp.Delete().Where(deviceprop.IDIn(in.Ids...)).Exec(l.ctx)

	if err != nil {
		return nil, dberrorhandler.DefaultEntError(l.Logger, err, in)
	}

	return &basic.BaseResp{Msg: i18n.DeleteSuccess}, nil
}
