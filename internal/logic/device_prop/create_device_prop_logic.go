package device_prop

import (
	"context"
	"github.com/suyuan32/simple-admin-common/i18n"

	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/svc"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/utils/dberrorhandler"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/types/basic"

	"github.com/suyuan32/simple-admin-common/utils/pointy"
	"github.com/zeromicro/go-zero/core/logx"
)

type CreateDevicePropLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewCreateDevicePropLogic(ctx context.Context, svcCtx *svc.ServiceContext) *CreateDevicePropLogic {
	return &CreateDevicePropLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *CreateDevicePropLogic) CreateDeviceProp(in *basic.DevicePropInfo) (*basic.BaseIDResp, error) {
	query := l.svcCtx.DB.DeviceProp.Create().
		SetNotNilSort(in.Sort).
		SetNotNilIotID(in.IotID).
		SetNotNilProps(in.Props)

	if in.Status != nil {
		query.SetNotNilStatus(pointy.GetPointer(uint8(*in.Status)))
	}

	result, err := query.Save(l.ctx)

	if err != nil {
		return nil, dberrorhandler.DefaultEntError(l.Logger, err, in)
	}

	return &basic.BaseIDResp{Id: result.ID, Msg: i18n.CreateSuccess}, nil
}
