package product_property

import (
	"context"

	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/svc"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/utils/dberrorhandler"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/types/basic"

	"github.com/suyuan32/simple-admin-common/utils/pointy"
	"github.com/zeromicro/go-zero/core/logx"
)

type GetProductPropertyByIdLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGetProductPropertyByIdLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetProductPropertyByIdLogic {
	return &GetProductPropertyByIdLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *GetProductPropertyByIdLogic) GetProductPropertyById(in *basic.IDReq) (*basic.ProductPropertyInfo, error) {
	result, err := l.svcCtx.DB.ProductProperty.Get(l.ctx, in.Id)
	if err != nil {
		return nil, dberrorhandler.DefaultEntError(l.Logger, err, in)
	}

	return &basic.ProductPropertyInfo{
		Id:          &result.ID,
		CreatedAt:    pointy.GetPointer(result.CreatedAt.UnixMilli()),
		UpdatedAt:    pointy.GetPointer(result.UpdatedAt.UnixMilli()),
		Status:	pointy.GetPointer(uint32(result.Status)),
		Sort:	&result.Sort,
		ProductKey:	&result.ProductKey,
		Version:	&result.Version,
		CreateTs:	&result.CreateTs,
		Identifier:	&result.Identifier,
		Name:	&result.Name,
		RwFlag:	&result.RwFlag,
		Required:	&result.Required,
		DataType:	&result.DataType,
		DataSpecs:	&result.DataSpecs,
		DataSpecsList:	&result.DataSpecsList,
		Custom:	&result.Custom,
	}, nil
}

