package product_property

import (
	"context"
	"github.com/suyuan32/simple-admin-common/i18n"

	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/svc"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/utils/dberrorhandler"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/types/basic"

	"github.com/suyuan32/simple-admin-common/utils/pointy"
	"github.com/zeromicro/go-zero/core/logx"
)

type UpdateProductPropertyLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewUpdateProductPropertyLogic(ctx context.Context, svcCtx *svc.ServiceContext) *UpdateProductPropertyLogic {
	return &UpdateProductPropertyLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *UpdateProductPropertyLogic) UpdateProductProperty(in *basic.ProductPropertyInfo) (*basic.BaseResp, error) {
	query := l.svcCtx.DB.ProductProperty.UpdateOneID(*in.Id).
		SetNotNilSort(in.Sort).
		SetNotNilProductKey(in.ProductKey).
		SetNotNilVersion(in.Version).
		SetNotNilCreateTs(in.CreateTs).
		SetNotNilIdentifier(in.Identifier).
		SetNotNilName(in.Name).
		SetNotNilRwFlag(in.RwFlag).
		SetNotNilRequired(in.Required).
		SetNotNilDataType(in.DataType).
		SetNotNilDataSpecs(in.DataSpecs).
		SetNotNilDataSpecsList(in.DataSpecsList).
		SetNotNilCustom(in.Custom)

	if in.Status != nil {
		query.SetNotNilStatus(pointy.GetPointer(uint8(*in.Status)))
	}

	err := query.Exec(l.ctx)

	if err != nil {
		return nil, dberrorhandler.DefaultEntError(l.Logger, err, in)
	}

	return &basic.BaseResp{Msg: i18n.UpdateSuccess}, nil
}
