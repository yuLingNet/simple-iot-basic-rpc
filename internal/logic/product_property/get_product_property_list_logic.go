package product_property

import (
	"context"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/ent"
	"strings"

	"gitee.com/yuLingNet/simple-iot-basic-rpc/ent/predicate"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/ent/productproperty"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/svc"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/utils/dberrorhandler"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/types/basic"

	"github.com/suyuan32/simple-admin-common/utils/pointy"
	"github.com/zeromicro/go-zero/core/logx"
)

type GetProductPropertyListLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGetProductPropertyListLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetProductPropertyListLogic {
	return &GetProductPropertyListLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *GetProductPropertyListLogic) GetProductPropertyList(in *basic.ProductPropertyListReq) (*basic.ProductPropertyListResp, error) {
	var predicates []predicate.ProductProperty
	if in.ProductKey != nil {
		predicates = append(predicates, productproperty.ProductKeyContains(*in.ProductKey))
	}
	if in.Version != nil {
		predicates = append(predicates, productproperty.VersionContains(*in.Version))
	}
	if in.Identifier != nil {
		predicates = append(predicates, productproperty.IdentifierContains(*in.Identifier))
	}
	result, err := l.svcCtx.DB.ProductProperty.Query().Where(predicates...).Page(l.ctx, in.Page, in.PageSize, func(pager *ent.ProductPropertyPager) {
		if in.Prop != nil && in.Order != nil {
			if *in.Order == "descending" {
				pager.Order = ent.Desc(camelCaseToUnderscore(*in.Prop))
			} else {
				pager.Order = ent.Asc(camelCaseToUnderscore(*in.Prop))
			}
		}
	})

	if err != nil {
		return nil, dberrorhandler.DefaultEntError(l.Logger, err, in)
	}

	resp := &basic.ProductPropertyListResp{}
	resp.Total = result.PageDetails.Total

	for _, v := range result.List {
		resp.Data = append(resp.Data, &basic.ProductPropertyInfo{
			Id:            &v.ID,
			CreatedAt:     pointy.GetPointer(v.CreatedAt.UnixMilli()),
			UpdatedAt:     pointy.GetPointer(v.UpdatedAt.UnixMilli()),
			Status:        pointy.GetPointer(uint32(v.Status)),
			Sort:          &v.Sort,
			ProductKey:    &v.ProductKey,
			Version:       &v.Version,
			CreateTs:      &v.CreateTs,
			Identifier:    &v.Identifier,
			Name:          &v.Name,
			RwFlag:        &v.RwFlag,
			Required:      &v.Required,
			DataType:      &v.DataType,
			DataSpecs:     &v.DataSpecs,
			DataSpecsList: &v.DataSpecsList,
			Custom:        &v.Custom,
		})
	}

	return resp, nil
}

func camelCaseToUnderscore(s string) string {
	data := make([]byte, 0, len(s)*2)
	j := false
	num := len(s)
	for i := 0; i < num; i++ {
		d := s[i]
		// or通过ASCII码进行大小写的转化
		// 65-90（A-Z），97-122（a-z）
		//判断如果字母为大写的A-Z就在前面拼接一个_
		if i > 0 && d >= 'A' && d <= 'Z' && j {
			data = append(data, '_')
		}
		if d != '_' {
			j = true
		}
		data = append(data, d)
	}
	//ToLower把大写字母统一转小写
	return strings.ToLower(string(data[:]))
}
