package thing_model

import (
	"context"
	"encoding/json"
	"gitee.com/yuLingNet/simple-drive-aliyun/aliyun"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/ent"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/ent/product"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/svc"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/types/basic"
	"github.com/suyuan32/simple-admin-common/i18n"

	"github.com/zeromicro/go-zero/core/logx"
)

type Property struct {
	ProductKey    string `json:"productKey"`
	CreateTs      int64  `json:"createTs"`
	Identifier    string `json:"identifier"`
	DataType      string `json:"dataType"`
	Name          string `json:"name"`
	RwFlag        string `json:"rwFlag"`
	DataSpecs     any    `json:"dataSpecs"`
	DataSpecsList any    `json:"dataSpecsList"`
	Required      bool   `json:"required"`
	Custom        bool   `json:"custom"`
}

type Event struct {
	ProductKey string `json:"productKey"`
	CreateTs   int64  `json:"createTs"`
	Identifier string `json:"identifier"`
	EventName  string `json:"eventName"`
	EventType  string `json:"eventType"`
	Outputdata any    `json:"outputdata"`
	Required   bool   `json:"required"`
	Custom     bool   `json:"custom"`
}

type Service struct {
	ProductKey   string `json:"productKey"`
	CreateTs     int64  `json:"createTs"`
	Identifier   string `json:"identifier"`
	ServiceName  string `json:"serviceName"`
	InputParams  any    `json:"inputParams"`
	OutputParams any    `json:"outputParams"`
	Required     bool   `json:"required"`
	CallType     string `json:"callType"`
	Custom       bool   `json:"custom"`
}

type ThingModel struct {
	Ppk struct {
		Version string `json:"version"`
	} `json:"_ppk"`
	Properties []Property `json:"properties"`
	Events     []Event    `json:"events"`
	Services   []Service  `json:"services"`
}

type SyncThingModelLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewSyncThingModelLogic(ctx context.Context, svcCtx *svc.ServiceContext) *SyncThingModelLogic {
	return &SyncThingModelLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *SyncThingModelLogic) SyncThingModel(in *basic.Empty) (*basic.BaseResp, error) {
	//获取产品ProductKey
	productKeys := l.svcCtx.DB.Product.Query().Select("product_key").StringsX(l.ctx)
	for _, key := range productKeys {
		//调用阿里云api
		err := QueryThingModel(key, l)
		if err != nil {
			return nil, err
		}
	}

	return &basic.BaseResp{Msg: i18n.Success}, nil
}

func QueryThingModel(productKey string, l *SyncThingModelLogic) error {
	thingModelResult, err := aliyun.QueryThingModel(productKey)
	if err != nil {
		return err
	}

	if thingModelResult.Data != nil {
		thingModelJson := thingModelResult.Data.ThingModelJson
		var thingModel ThingModel
		_ = json.Unmarshal([]byte(*thingModelJson), &thingModel)
		product, _ := l.svcCtx.DB.Product.Query().Where(product.ProductKeyEQ(productKey)).First(l.ctx)

		//Properties
		if thingModel.Properties != nil {
			bulk := make([]*ent.ProductPropertyCreate, len(thingModel.Properties))

			for i, property := range thingModel.Properties {
				DataSpecs, _ := json.Marshal(property.DataSpecs)
				if err != nil {
					//fmt.Println("Error marshalling map to JSON:", err)
					return err
				}
				DataSpecsString := string(DataSpecs)
				DataSpecsList, _ := json.Marshal(property.DataSpecsList)
				if err != nil {
					//fmt.Println("Error marshalling map to JSON:", err)
					return err
				}
				DataSpecsListString := string(DataSpecsList)
				bulk[i] = l.svcCtx.DB.ProductProperty.Create().
					SetNotNilProductKey(&productKey).
					SetNotNilCreateTs(&property.CreateTs).
					SetNotNilVersion(&thingModel.Ppk.Version).
					SetNotNilIdentifier(&property.Identifier).
					SetNotNilDataType(&property.DataType).
					SetNotNilName(&property.Name).
					SetNotNilRwFlag(&property.RwFlag).
					SetNotNilDataSpecs(&DataSpecsString).
					SetNotNilDataSpecsList(&DataSpecsListString).
					SetNotNilRequired(&property.Required).
					SetNotNilCustom(&property.Custom).
					SetProduct(product)
			}
			err := l.svcCtx.DB.ProductProperty.CreateBulk(bulk...).
				OnConflict().
				UpdateNewValues().
				Exec(l.ctx)
			if err != nil {
				return err
			}
		}

		//Events
		if thingModel.Events != nil {
			bulk := make([]*ent.ProductEventCreate, len(thingModel.Events))
			for i, event := range thingModel.Events {
				Outputdata, _ := json.Marshal(event.Outputdata)
				if err != nil {
					//fmt.Println("Error marshalling map to JSON:", err)
					return err
				}
				OutputdataString := string(Outputdata)
				bulk[i] = l.svcCtx.DB.ProductEvent.Create().
					SetNotNilProductKey(&productKey).
					SetNotNilCreateTs(&event.CreateTs).
					SetNotNilVersion(&thingModel.Ppk.Version).
					SetNotNilIdentifier(&event.Identifier).
					SetNotNilEventName(&event.EventName).
					SetNotNilEventType(&event.EventType).
					SetNotNilOutputData(&OutputdataString).
					SetNotNilRequired(&event.Required).
					SetNotNilCustom(&event.Custom).
					SetProduct(product)
			}
			err := l.svcCtx.DB.ProductEvent.CreateBulk(bulk...).
				OnConflict().
				UpdateNewValues().
				Exec(l.ctx)
			if err != nil {
				return err
			}
		}

		//Services
		if thingModel.Services != nil {
			bulk := make([]*ent.ProductServiceCreate, len(thingModel.Services))
			for i, service := range thingModel.Services {
				InputParams, _ := json.Marshal(service.InputParams)
				if err != nil {
					//fmt.Println("Error marshalling map to JSON:", err)
					return err
				}
				InputParamsString := string(InputParams)
				OutputParams, _ := json.Marshal(service.OutputParams)
				if err != nil {
					//fmt.Println("Error marshalling map to JSON:", err)
					return err
				}
				OutputParamsString := string(OutputParams)
				bulk[i] = l.svcCtx.DB.ProductService.Create().
					SetNotNilProductKey(&productKey).
					SetNotNilCreateTs(&service.CreateTs).
					SetNotNilVersion(&thingModel.Ppk.Version).
					SetNotNilIdentifier(&service.Identifier).
					SetNotNilServiceName(&service.ServiceName).
					SetNotNilInputParams(&InputParamsString).
					SetNotNilOutputParams(&OutputParamsString).
					SetNotNilRequired(&service.Required).
					SetNotNilCallType(&service.CallType).
					SetNotNilCustom(&service.Custom).
					SetProduct(product)
			}
			err := l.svcCtx.DB.ProductService.CreateBulk(bulk...).
				OnConflict().
				UpdateNewValues().
				Exec(l.ctx)
			if err != nil {
				return err
			}
		}
	}
	return nil
}
