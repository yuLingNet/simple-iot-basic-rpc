package base

import (
	"github.com/suyuan32/simple-admin-common/enum/common"
	"github.com/suyuan32/simple-admin-core/rpc/ent"
	"github.com/zeromicro/go-zero/core/errorx"
	"github.com/zeromicro/go-zero/core/logx"
	"time"
)

// init menu data
func (l *InitDatabaseLogic) insertMenuData() error {
	_, err := l.svcCtx.DBCore.Menu.Delete().Exec(l.ctx)
	if err != nil {
		logx.Errorw(err.Error())
		return errorx.NewInternalError(err.Error())
	}

	var menus []*ent.MenuCreate

	menus = append(menus, l.svcCtx.DBCore.Menu.Create().
		SetID(1).
		SetCreatedAt(time.Now()).
		SetUpdatedAt(time.Now()).
		SetSort(1).
		SetMenuLevel(1).
		SetMenuType(1).
		SetParentID(common.DefaultParentId).
		SetPath("/dashboard").
		SetName("Dashboard").
		SetComponent("home/index").
		SetTitle("menu.dashboard").
		SetIcon("el-icon-home-filled").
		SetHideMenu(false).
		SetServiceName("Core"),
	)

	menus = append(menus, l.svcCtx.DBCore.Menu.Create().
		SetID(2).
		SetCreatedAt(time.Now()).
		SetUpdatedAt(time.Now()).
		SetSort(999).
		SetMenuLevel(1).
		SetMenuType(0).
		SetParentID(common.DefaultParentId).
		SetPath("/system").
		SetName("SystemManagement").
		SetComponent("home/index").
		SetTitle("menu.system").
		SetIcon("el-icon-menu").
		SetHideMenu(false).
		SetServiceName("Core"),
	)

	menus = append(menus, l.svcCtx.DBCore.Menu.Create().
		SetID(3).
		SetCreatedAt(time.Now()).
		SetUpdatedAt(time.Now()).
		SetSort(1).
		SetMenuLevel(2).
		SetMenuType(1).
		SetParentID(2).
		SetPath("/core/menu").
		SetName("MenuManagement").
		SetComponent("core/menu").
		SetTitle("menu.menu").
		SetIcon("el-icon-menu").
		SetHideMenu(false).
		SetServiceName("Core"),
	)

	menus = append(menus, l.svcCtx.DBCore.Menu.Create().
		SetID(4).
		SetCreatedAt(time.Now()).
		SetUpdatedAt(time.Now()).
		SetSort(2).
		SetMenuLevel(2).
		SetMenuType(0).
		SetParentID(2).
		SetPath("/user").
		SetName("User").
		SetComponent("core/user").
		SetTitle("menu.user").
		SetIcon("el-icon-avatar").
		SetHideMenu(false).
		SetServiceName("Core"),
	)

	menus = append(menus, l.svcCtx.DBCore.Menu.Create().
		SetID(5).
		SetCreatedAt(time.Now()).
		SetUpdatedAt(time.Now()).
		SetSort(1).
		SetMenuLevel(3).
		SetMenuType(1).
		SetParentID(4).
		SetPath("/core/role").
		SetName("RoleManagement").
		SetComponent("core/role").
		SetTitle("menu.role").
		SetIcon("el-icon-coordinate").
		SetHideMenu(false).
		SetServiceName("Core"),
	)

	menus = append(menus, l.svcCtx.DBCore.Menu.Create().
		SetID(6).
		SetCreatedAt(time.Now()).
		SetUpdatedAt(time.Now()).
		SetSort(2).
		SetMenuLevel(3).
		SetMenuType(1).
		SetParentID(4).
		SetPath("/core/user").
		SetName("UserManagement").
		SetComponent("core/user").
		SetTitle("menu.user").
		SetIcon("el-icon-avatar").
		SetHideMenu(false).
		SetServiceName("Core"),
	)

	menus = append(menus, l.svcCtx.DBCore.Menu.Create().
		SetID(7).
		SetCreatedAt(time.Now()).
		SetUpdatedAt(time.Now()).
		SetSort(3).
		SetMenuLevel(3).
		SetMenuType(1).
		SetParentID(4).
		SetPath("/core/department").
		SetName("DepartmentManagement").
		SetComponent("core/department").
		SetTitle("menu.department").
		SetIcon("el-icon-box").
		SetHideMenu(false).
		SetServiceName("Core"),
	)

	menus = append(menus, l.svcCtx.DBCore.Menu.Create().
		SetID(8).
		SetCreatedAt(time.Now()).
		SetUpdatedAt(time.Now()).
		SetSort(4).
		SetMenuLevel(3).
		SetMenuType(1).
		SetParentID(4).
		SetPath("/core/oauth").
		SetName("OauthManagement").
		SetComponent("core/oauth").
		SetTitle("menu.oauth").
		SetIcon("sc-icon-wechat").
		SetHideMenu(false).
		SetServiceName("Core"),
	)

	menus = append(menus, l.svcCtx.DBCore.Menu.Create().
		SetID(9).
		SetCreatedAt(time.Now()).
		SetUpdatedAt(time.Now()).
		SetSort(5).
		SetMenuLevel(3).
		SetMenuType(1).
		SetParentID(4).
		SetPath("/core/token").
		SetName("TokenManagement").
		SetComponent("core/token").
		SetTitle("menu.token").
		SetIcon("el-icon-discount").
		SetHideMenu(false).
		SetServiceName("Core"),
	)

	menus = append(menus, l.svcCtx.DBCore.Menu.Create().
		SetID(10).
		SetCreatedAt(time.Now()).
		SetUpdatedAt(time.Now()).
		SetSort(6).
		SetMenuLevel(3).
		SetMenuType(1).
		SetParentID(4).
		SetPath("/core/position").
		SetName("PositionManagement").
		SetComponent("core/position").
		SetTitle("menu.position").
		SetIcon("el-icon-brush-filled").
		SetHideMenu(false).
		SetServiceName("Core"),
	)

	menus = append(menus, l.svcCtx.DBCore.Menu.Create().
		SetID(11).
		SetCreatedAt(time.Now()).
		SetUpdatedAt(time.Now()).
		SetSort(3).
		SetMenuLevel(2).
		SetMenuType(1).
		SetParentID(2).
		SetPath("/core/api").
		SetName("APIManagement").
		SetComponent("core/api").
		SetTitle("menu.api").
		SetIcon("el-icon-cpu").
		SetHideMenu(false).
		SetServiceName("Core"),
	)

	menus = append(menus, l.svcCtx.DBCore.Menu.Create().
		SetID(12).
		SetCreatedAt(time.Now()).
		SetUpdatedAt(time.Now()).
		SetSort(4).
		SetMenuLevel(2).
		SetMenuType(1).
		SetParentID(2).
		SetPath("/core/dictionary").
		SetName("DictionaryManagement").
		SetComponent("core/dictionary").
		SetTitle("menu.dictionary").
		SetIcon("el-icon-coin").
		SetHideMenu(false).
		SetServiceName("Core"),
	)

	menus = append(menus, l.svcCtx.DBCore.Menu.Create().
		SetID(13).
		SetCreatedAt(time.Now()).
		SetUpdatedAt(time.Now()).
		SetSort(5).
		SetMenuLevel(2).
		SetMenuType(1).
		SetParentID(2).
		SetPath("/mcms/email_provider").
		SetName("EmailProviderManagement").
		SetComponent("mcms/emailProvider").
		SetTitle("menu.email").
		SetIcon("el-icon-document-copy").
		SetHideMenu(false).
		SetServiceName("Mcms"),
	)

	menus = append(menus, l.svcCtx.DBCore.Menu.Create().
		SetID(14).
		SetCreatedAt(time.Now()).
		SetUpdatedAt(time.Now()).
		SetSort(6).
		SetMenuLevel(2).
		SetMenuType(1).
		SetParentID(2).
		SetPath("/mcms/sms_provider").
		SetName("SmsProviderManagement").
		SetComponent("mcms/smsProvider").
		SetTitle("menu.sms").
		SetIcon("el-icon-chat-dot-square").
		SetHideMenu(false).
		SetServiceName("Mcms"),
	)

	menus = append(menus, l.svcCtx.DBCore.Menu.Create().
		SetID(15).
		SetCreatedAt(time.Now()).
		SetUpdatedAt(time.Now()).
		SetSort(7).
		SetMenuLevel(2).
		SetMenuType(1).
		SetParentID(2).
		SetPath("/core/profile").
		SetName("Profile").
		SetComponent("core/user/profile").
		SetTitle("menu.profile").
		SetIcon("el-icon-user").
		SetHideMenu(false).
		SetServiceName("Core"),
	)

	menus = append(menus, l.svcCtx.DBCore.Menu.Create().
		SetID(16).
		SetCreatedAt(time.Now()).
		SetUpdatedAt(time.Now()).
		SetSort(998).
		SetMenuLevel(1).
		SetMenuType(1).
		SetParentID(common.DefaultParentId).
		SetPath("/job/task").
		SetName("TaskManagement").
		SetComponent("job/task").
		SetTitle("menu.job").
		SetIcon("el-icon-alarm-clock").
		SetHideMenu(false).
		SetServiceName("Job"),
	)

	menus = append(menus, l.svcCtx.DBCore.Menu.Create().
		SetID(17).
		SetCreatedAt(time.Now()).
		SetUpdatedAt(time.Now()).
		SetSort(2).
		SetMenuLevel(1).
		SetMenuType(0).
		SetParentID(common.DefaultParentId).
		SetPath("/equip").
		SetName("Equip").
		SetComponent("").
		SetTitle("menu.equip").
		SetIcon("el-icon-cpu").
		SetHideMenu(false).
		SetServiceName("Basic"),
	)

	menus = append(menus, l.svcCtx.DBCore.Menu.Create().
		SetID(18).
		SetCreatedAt(time.Now()).
		SetUpdatedAt(time.Now()).
		SetSort(1).
		SetMenuLevel(2).
		SetMenuType(1).
		SetParentID(17).
		SetPath("/equip/product").
		SetName("Product").
		SetComponent("equip/product").
		SetTitle("menu.product").
		SetIcon("el-icon-document-copy").
		SetHideMenu(false).
		SetServiceName("Basic"),
	)

	menus = append(menus, l.svcCtx.DBCore.Menu.Create().
		SetID(19).
		SetCreatedAt(time.Now()).
		SetUpdatedAt(time.Now()).
		SetSort(1).
		SetMenuLevel(2).
		SetMenuType(1).
		SetParentID(17).
		SetPath("/equip/product/detail").
		SetName("ProductDetail").
		SetComponent("equip/product/detail").
		SetTitle("menu.detail").
		SetIcon("").
		SetHideMenu(true).
		SetServiceName("Basic"),
	)

	menus = append(menus, l.svcCtx.DBCore.Menu.Create().
		SetID(20).
		SetCreatedAt(time.Now()).
		SetUpdatedAt(time.Now()).
		SetSort(2).
		SetMenuLevel(2).
		SetMenuType(1).
		SetParentID(17).
		SetPath("/equip/device").
		SetName("Device").
		SetComponent("equip/device").
		SetTitle("menu.device").
		SetIcon("el-icon-printer").
		SetHideMenu(false).
		SetServiceName("Basic"),
	)

	menus = append(menus, l.svcCtx.DBCore.Menu.Create().
		SetID(21).
		SetCreatedAt(time.Now()).
		SetUpdatedAt(time.Now()).
		SetSort(2).
		SetMenuLevel(2).
		SetMenuType(1).
		SetParentID(17).
		SetPath("/equip/device/detail").
		SetName("DeviceDetail").
		SetComponent("equip/device/detail").
		SetTitle("menu.detail").
		SetIcon("").
		SetHideMenu(true).
		SetServiceName("Basic"),
	)

	menus = append(menus, l.svcCtx.DBCore.Menu.Create().
		SetID(22).
		SetCreatedAt(time.Now()).
		SetUpdatedAt(time.Now()).
		SetSort(2).
		SetMenuLevel(2).
		SetMenuType(1).
		SetParentID(17).
		SetPath("/equip/device/event").
		SetName("DeviceEvent").
		SetComponent("equip/device/event").
		SetTitle("menu.event").
		SetIcon("").
		SetHideMenu(true).
		SetServiceName("Basic"),
	)

	menus = append(menus, l.svcCtx.DBCore.Menu.Create().
		SetID(23).
		SetCreatedAt(time.Now()).
		SetUpdatedAt(time.Now()).
		SetSort(2).
		SetMenuLevel(2).
		SetMenuType(1).
		SetParentID(17).
		SetPath("/equip/device/property").
		SetName("DeviceProperty").
		SetComponent("equip/device/property").
		SetTitle("menu.property").
		SetIcon("").
		SetHideMenu(true).
		SetServiceName("Basic"),
	)

	menus = append(menus, l.svcCtx.DBCore.Menu.Create().
		SetID(24).
		SetCreatedAt(time.Now()).
		SetUpdatedAt(time.Now()).
		SetSort(3).
		SetMenuLevel(1).
		SetMenuType(0).
		SetParentID(common.DefaultParentId).
		SetPath("/client").
		SetName("Client").
		SetComponent("").
		SetTitle("menu.client").
		SetIcon("el-icon-avatar").
		SetHideMenu(false).
		SetServiceName("Basic"),
	)

	menus = append(menus, l.svcCtx.DBCore.Menu.Create().
		SetID(25).
		SetCreatedAt(time.Now()).
		SetUpdatedAt(time.Now()).
		SetSort(1).
		SetMenuLevel(2).
		SetMenuType(1).
		SetParentID(24).
		SetPath("/client/company").
		SetName("ClientCompany").
		SetComponent("client/company").
		SetTitle("menu.company").
		SetIcon("el-icon-goods").
		SetHideMenu(false).
		SetServiceName("Basic"),
	)

	menus = append(menus, l.svcCtx.DBCore.Menu.Create().
		SetID(26).
		SetCreatedAt(time.Now()).
		SetUpdatedAt(time.Now()).
		SetSort(2).
		SetMenuLevel(2).
		SetMenuType(1).
		SetParentID(24).
		SetPath("/client/member").
		SetName("ClientMember").
		SetComponent("client/member").
		SetTitle("menu.member").
		SetIcon("el-icon-avatar").
		SetHideMenu(false).
		SetServiceName("Basic"),
	)

	menus = append(menus, l.svcCtx.DBCore.Menu.Create().
		SetID(27).
		SetCreatedAt(time.Now()).
		SetUpdatedAt(time.Now()).
		SetSort(2).
		SetMenuLevel(2).
		SetMenuType(1).
		SetParentID(24).
		SetPath("/client/company/bindlist").
		SetName("ClientCompanyBind").
		SetComponent("client/company/bindlist").
		SetTitle("menu.bindlist").
		SetIcon("").
		SetHideMenu(true).
		SetServiceName("Basic"),
	)

	menus = append(menus, l.svcCtx.DBCore.Menu.Create().
		SetID(28).
		SetCreatedAt(time.Now()).
		SetUpdatedAt(time.Now()).
		SetSort(2).
		SetMenuLevel(2).
		SetMenuType(1).
		SetParentID(24).
		SetPath("/client/member/bindlist").
		SetName("ClientMemberBind").
		SetComponent("client/member/bindlist").
		SetTitle("menu.bindlist").
		SetIcon("").
		SetHideMenu(true).
		SetServiceName("Basic"),
	)

	menus = append(menus, l.svcCtx.DBCore.Menu.Create().
		SetID(29).
		SetCreatedAt(time.Now()).
		SetUpdatedAt(time.Now()).
		SetSort(4).
		SetMenuLevel(1).
		SetMenuType(0).
		SetParentID(common.DefaultParentId).
		SetPath("/statistics").
		SetName("Statistics").
		SetComponent("").
		SetTitle("menu.statistics").
		SetIcon("el-icon-document").
		SetHideMenu(false).
		SetServiceName("Basic"),
	)

	menus = append(menus, l.svcCtx.DBCore.Menu.Create().
		SetID(30).
		SetCreatedAt(time.Now()).
		SetUpdatedAt(time.Now()).
		SetSort(1).
		SetMenuLevel(2).
		SetMenuType(1).
		SetParentID(29).
		SetPath("/statistics/upster/cycle").
		SetName("UpsterCycle").
		SetComponent("statistics/upster").
		SetTitle("menu.upstercycle").
		SetIcon("el-icon-document-remove").
		SetHideMenu(false).
		SetServiceName("Data"),
	)

	menus = append(menus, l.svcCtx.DBCore.Menu.Create().
		SetID(31).
		SetCreatedAt(time.Now()).
		SetUpdatedAt(time.Now()).
		SetSort(2).
		SetMenuLevel(2).
		SetMenuType(1).
		SetParentID(29).
		SetPath("/statistics/upster/statistical").
		SetName("Upsterstatistical").
		SetComponent("statistics/upster/statistical").
		SetTitle("menu.statistical").
		SetIcon("el-icon-document-remove").
		SetHideMenu(false).
		SetServiceName("Data"),
	)

	menus = append(menus, l.svcCtx.DBCore.Menu.Create().
		SetID(32).
		SetCreatedAt(time.Now()).
		SetUpdatedAt(time.Now()).
		SetSort(3).
		SetMenuLevel(2).
		SetMenuType(1).
		SetParentID(29).
		SetPath("/statistics/mer").
		SetName("MerDuration").
		SetComponent("statistics/mer").
		SetTitle("menu.mer").
		SetIcon("el-icon-document-remove").
		SetHideMenu(false).
		SetServiceName("Data"),
	)

	menus = append(menus, l.svcCtx.DBCore.Menu.Create().
		SetID(33).
		SetCreatedAt(time.Now()).
		SetUpdatedAt(time.Now()).
		SetSort(4).
		SetMenuLevel(2).
		SetMenuType(1).
		SetParentID(29).
		SetPath("/statistics/elephant").
		SetName("ElephantDuration").
		SetComponent("statistics/elephant").
		SetTitle("menu.elephant").
		SetIcon("el-icon-document-remove").
		SetHideMenu(false).
		SetServiceName("Data"),
	)

	err = l.svcCtx.DBCore.Menu.CreateBulk(menus...).Exec(l.ctx)
	if err != nil {
		logx.Errorw(err.Error())
		return errorx.NewInternalError(err.Error())
	} else {
		return nil
	}
}
