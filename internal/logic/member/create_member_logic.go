package member

import (
	"context"
	"github.com/suyuan32/simple-admin-common/i18n"

	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/svc"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/utils/dberrorhandler"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/types/basic"

	"github.com/suyuan32/simple-admin-common/utils/pointy"
	"github.com/zeromicro/go-zero/core/logx"
)

type CreateMemberLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewCreateMemberLogic(ctx context.Context, svcCtx *svc.ServiceContext) *CreateMemberLogic {
	return &CreateMemberLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *CreateMemberLogic) CreateMember(in *basic.MemberInfo) (*basic.BaseUUIDResp, error) {
	query := l.svcCtx.DB.Member.Create().
		SetNotNilSort(in.Sort).
		SetNotNilCompanyID(in.CompanyID).
		SetNotNilType(in.Type).
		SetNotNilName(in.Name).
		SetNotNilMobile(in.Mobile).
		SetNotNilAvatar(in.Avatar).
		SetNotNilAccount(in.Account).
		SetNotNilPassword(in.Password).
		SetNotNilCountryCode(in.CountryCode)

	if in.Status != nil {
		query.SetNotNilStatus(pointy.GetPointer(uint8(*in.Status)))
	}

	result, err := query.Save(l.ctx)

	if err != nil {
		return nil, dberrorhandler.DefaultEntError(l.Logger, err, in)
	}

	return &basic.BaseUUIDResp{Id: result.ID.String(), Msg: i18n.CreateSuccess}, nil
}
