package member

import (
	"context"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/ent/member"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/svc"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/utils/dberrorhandler"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/types/basic"
	"github.com/suyuan32/simple-admin-common/utils/pointy"

	"github.com/zeromicro/go-zero/core/logx"
)

type GetMemberByMobileLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGetMemberByMobileLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetMemberByMobileLogic {
	return &GetMemberByMobileLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *GetMemberByMobileLogic) GetMemberByMobile(in *basic.MobileReq) (*basic.MemberInfo, error) {
	result, err := l.svcCtx.DB.Member.Query().Where(member.MobileContains(in.Mobile)).First(l.ctx)
	if err != nil {
		return nil, dberrorhandler.DefaultEntError(l.Logger, err, in)
	}

	return &basic.MemberInfo{
		Id:          pointy.GetPointer(result.ID.String()),
		CreatedAt:   pointy.GetPointer(result.CreatedAt.UnixMilli()),
		UpdatedAt:   pointy.GetPointer(result.UpdatedAt.UnixMilli()),
		Status:      pointy.GetPointer(uint32(result.Status)),
		Sort:        &result.Sort,
		CompanyID:   &result.CompanyID,
		Type:        &result.Type,
		Name:        &result.Name,
		Mobile:      &result.Mobile,
		Avatar:      &result.Avatar,
		Account:     &result.Account,
		Password:    &result.Password,
		CountryCode: &result.CountryCode,
	}, nil
}
