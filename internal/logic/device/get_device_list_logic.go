package device

import (
	"context"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/ent"
	"strings"

	"gitee.com/yuLingNet/simple-iot-basic-rpc/ent/device"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/ent/predicate"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/svc"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/utils/dberrorhandler"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/types/basic"

	"github.com/suyuan32/simple-admin-common/utils/pointy"
	"github.com/zeromicro/go-zero/core/logx"
)

type GetDeviceListLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGetDeviceListLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetDeviceListLogic {
	return &GetDeviceListLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *GetDeviceListLogic) GetDeviceList(in *basic.DeviceListReq) (*basic.DeviceListResp, error) {
	var predicates []predicate.Device
	if in.ProductKey != nil {
		predicates = append(predicates, device.ProductKeyContains(*in.ProductKey))
	}
	if in.IotID != nil {
		predicates = append(predicates, device.IotIDContains(*in.IotID))
	}
	if in.DeviceName != nil {
		predicates = append(predicates, device.DeviceNameContains(*in.DeviceName))
	}
	if in.DeviceStatus != nil {
		predicates = append(predicates, device.DeviceStatusContains(*in.DeviceStatus))
	}
	if len(in.IotIDs) > 0 {
		predicates = append(predicates, device.IotIDIn(in.IotIDs...))
	}
	if in.Tag != nil && *in.Tag != 0 {
		predicates = append(predicates, device.TagEQ(*in.Tag))
	}
	if len(in.ProductKeys) > 0 {
		predicates = append(predicates, device.ProductKeyIn(in.ProductKeys...))
	}
	result, err := l.svcCtx.DB.Device.Query().Where(predicates...).Page(l.ctx, in.Page, in.PageSize, func(pager *ent.DevicePager) {
		if in.Prop != nil && in.Order != nil {
			if *in.Order == "descending" {
				pager.Order = ent.Desc(camelCaseToUnderscore(*in.Prop))
			} else {
				pager.Order = ent.Asc(camelCaseToUnderscore(*in.Prop))
			}
		}
	})

	if err != nil {
		return nil, dberrorhandler.DefaultEntError(l.Logger, err, in)
	}

	resp := &basic.DeviceListResp{}
	resp.Total = result.PageDetails.Total

	for _, v := range result.List {
		resp.Data = append(resp.Data, &basic.DeviceInfo{
			Id:                    pointy.GetPointer(v.ID.String()),
			CreatedAt:             pointy.GetPointer(v.CreatedAt.UnixMilli()),
			UpdatedAt:             pointy.GetPointer(v.UpdatedAt.UnixMilli()),
			Status:                pointy.GetPointer(uint32(v.Status)),
			Sort:                  &v.Sort,
			ProductKey:            &v.ProductKey,
			IotID:                 &v.IotID,
			DeviceName:            &v.DeviceName,
			DeviceSecret:          &v.DeviceSecret,
			DeviceStatus:          &v.DeviceStatus,
			Nickname:              &v.Nickname,
			GmtCreate:             &v.GmtCreate,
			GmtModified:           &v.GmtModified,
			UtcCreate:             &v.UtcCreate,
			UtcModified:           &v.UtcModified,
			OperationalStatusCode: &v.OperationalStatusCode,
			FirmwareVersion:       &v.FirmwareVersion,
			GmtActive:             &v.GmtActive,
			GmtOnline:             &v.GmtOnline,
			IPAddress:             &v.IPAddress,
			NodeType:              &v.NodeType,
			Owner:                 &v.Owner,
			ProductName:           &v.ProductName,
			Region:                &v.Region,
			UtcActive:             &v.UtcActive,
			UtcOnline:             &v.UtcOnline,
			Tag:                   &v.Tag,
		})
	}

	return resp, nil
}

func camelCaseToUnderscore(s string) string {
	data := make([]byte, 0, len(s)*2)
	j := false
	num := len(s)
	for i := 0; i < num; i++ {
		d := s[i]
		// or通过ASCII码进行大小写的转化
		// 65-90（A-Z），97-122（a-z）
		//判断如果字母为大写的A-Z就在前面拼接一个_
		if i > 0 && d >= 'A' && d <= 'Z' && j {
			data = append(data, '_')
		}
		if d != '_' {
			j = true
		}
		data = append(data, d)
	}
	//ToLower把大写字母统一转小写
	return strings.ToLower(string(data[:]))
}
