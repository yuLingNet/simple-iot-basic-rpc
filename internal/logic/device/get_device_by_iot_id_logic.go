package device

import (
	"context"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/ent/device"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/svc"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/utils/dberrorhandler"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/types/basic"
	"github.com/suyuan32/simple-admin-common/utils/pointy"

	"github.com/zeromicro/go-zero/core/logx"
)

type GetDeviceByIotIdLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGetDeviceByIotIdLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetDeviceByIotIdLogic {
	return &GetDeviceByIotIdLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *GetDeviceByIotIdLogic) GetDeviceByIotId(in *basic.DeviceIotIdReq) (*basic.DeviceInfo, error) {
	result, err := l.svcCtx.DB.Device.Query().Where(device.IotIDEQ(in.IotId)).First(l.ctx)
	if err != nil {
		return nil, dberrorhandler.DefaultEntError(l.Logger, err, in)
	}

	return &basic.DeviceInfo{
		Id:                    pointy.GetPointer(result.ID.String()),
		CreatedAt:             pointy.GetPointer(result.CreatedAt.UnixMilli()),
		UpdatedAt:             pointy.GetPointer(result.UpdatedAt.UnixMilli()),
		Status:                pointy.GetPointer(uint32(result.Status)),
		Sort:                  &result.Sort,
		ProductKey:            &result.ProductKey,
		IotID:                 &result.IotID,
		DeviceName:            &result.DeviceName,
		DeviceSecret:          &result.DeviceSecret,
		DeviceStatus:          &result.DeviceStatus,
		Nickname:              &result.Nickname,
		GmtCreate:             &result.GmtCreate,
		GmtModified:           &result.GmtModified,
		UtcCreate:             &result.UtcCreate,
		UtcModified:           &result.UtcModified,
		OperationalStatusCode: &result.OperationalStatusCode,
		FirmwareVersion:       &result.FirmwareVersion,
		GmtActive:             &result.GmtActive,
		GmtOnline:             &result.GmtOnline,
		IPAddress:             &result.IPAddress,
		NodeType:              &result.NodeType,
		Owner:                 &result.Owner,
		ProductName:           &result.ProductName,
		Region:                &result.Region,
		UtcActive:             &result.UtcActive,
		UtcOnline:             &result.UtcOnline,
	}, nil
}
