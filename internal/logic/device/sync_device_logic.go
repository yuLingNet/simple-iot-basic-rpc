package device

import (
	"context"
	"gitee.com/yuLingNet/simple-drive-aliyun/aliyun"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/ent"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/ent/product"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/svc"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/types/basic"
	"github.com/suyuan32/simple-admin-common/i18n"

	"github.com/zeromicro/go-zero/core/logx"
)

type SyncDeviceLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewSyncDeviceLogic(ctx context.Context, svcCtx *svc.ServiceContext) *SyncDeviceLogic {
	return &SyncDeviceLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *SyncDeviceLogic) SyncDevice(in *basic.SyncDeviceReq) (*basic.BaseResp, error) {
	//调用阿里云api
	if in.ProductKey != nil {
		err := QueryDevice(1, 20, *in.ProductKey, l)
		if err != nil {
			return nil, err
		}
	} else {
		//获取产品ProductKey
		productKeys := l.svcCtx.DB.Product.Query().Select("product_key").StringsX(l.ctx)
		for _, key := range productKeys {
			err := QueryDevice(1, 20, key, l)
			if err != nil {
				return nil, err
			}
		}
	}

	return &basic.BaseResp{Msg: i18n.Success}, nil
}

func QueryDevice(page int32, size int32, productKey string, l *SyncDeviceLogic) error {
	deviceResult, err := aliyun.QueryDevice(page, size, productKey, "")
	if err != nil {
		if err != nil {
			return err
		}
	}

	if deviceResult.Data != nil {
		list := deviceResult.Data.DeviceInfo
		if len(list) > 0 {
			product, _ := l.svcCtx.DB.Product.Query().Where(product.ProductKeyEQ(productKey)).First(l.ctx)
			bulk := make([]*ent.DeviceCreate, len(list))
			for i, item := range list {
				deviceDetailResult, err := aliyun.QueryDeviceDetail(*item.IotId)
				if err != nil {
					return err
				}
				detail := deviceDetailResult.Data

				bulk[i] = l.svcCtx.DB.Device.Create().
					SetNotNilProductKey(item.ProductKey).
					SetNotNilIotID(item.IotId).
					SetNotNilDeviceName(item.DeviceName).
					SetNotNilDeviceSecret(item.DeviceSecret).
					SetNotNilDeviceStatus(item.DeviceStatus).
					SetNotNilNickname(item.Nickname).
					SetNotNilGmtCreate(item.GmtCreate).
					SetNotNilGmtModified(item.GmtModified).
					SetNotNilGmtActive(detail.GmtActive).
					SetNotNilGmtOnline(detail.GmtOnline).
					SetNotNilUtcCreate(item.UtcCreate).
					SetNotNilUtcModified(item.UtcModified).
					SetNotNilUtcActive(detail.UtcActive).
					SetNotNilUtcOnline(detail.UtcOnline).
					SetNotNilFirmwareVersion(detail.FirmwareVersion).
					SetNotNilIPAddress(detail.IpAddress).
					SetNotNilNodeType(detail.NodeType).
					SetNotNilOwner(detail.Owner).
					SetNotNilProductName(detail.ProductName).
					SetNotNilRegion(detail.Region).
					SetProduct(product)
			}
			err := l.svcCtx.DB.Device.CreateBulk(bulk...).
				OnConflict().
				UpdateNewValues().
				Exec(l.ctx)
			if err != nil {
				return err
			}
		}

		if *deviceResult.PageCount > 1 && page < *deviceResult.PageCount {
			page++
			err := QueryDevice(page, size, productKey, l)
			if err != nil {
				return err
			}
		}
	}
	return nil
}
