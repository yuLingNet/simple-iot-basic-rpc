package device

import (
	"context"
	"github.com/suyuan32/simple-admin-common/i18n"

	"gitee.com/yuLingNet/simple-iot-basic-rpc/ent/device"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/svc"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/utils/dberrorhandler"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/types/basic"

	"github.com/suyuan32/simple-admin-common/utils/uuidx"
	"github.com/zeromicro/go-zero/core/logx"
)

type DeleteDeviceLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewDeleteDeviceLogic(ctx context.Context, svcCtx *svc.ServiceContext) *DeleteDeviceLogic {
	return &DeleteDeviceLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *DeleteDeviceLogic) DeleteDevice(in *basic.UUIDsReq) (*basic.BaseResp, error) {
	_, err := l.svcCtx.DB.Device.Delete().Where(device.IDIn(uuidx.ParseUUIDSlice(in.Ids)...)).Exec(l.ctx)

	if err != nil {
		return nil, dberrorhandler.DefaultEntError(l.Logger, err, in)
	}

	return &basic.BaseResp{Msg: i18n.DeleteSuccess}, nil
}
