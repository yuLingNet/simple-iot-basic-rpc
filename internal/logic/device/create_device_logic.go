package device

import (
	"context"
	"github.com/suyuan32/simple-admin-common/i18n"

	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/svc"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/utils/dberrorhandler"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/types/basic"

	"github.com/suyuan32/simple-admin-common/utils/pointy"
	"github.com/zeromicro/go-zero/core/logx"
)

type CreateDeviceLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewCreateDeviceLogic(ctx context.Context, svcCtx *svc.ServiceContext) *CreateDeviceLogic {
	return &CreateDeviceLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *CreateDeviceLogic) CreateDevice(in *basic.DeviceInfo) (*basic.BaseUUIDResp, error) {
	query := l.svcCtx.DB.Device.Create().
		SetNotNilSort(in.Sort).
		SetNotNilProductKey(in.ProductKey).
		SetNotNilIotID(in.IotID).
		SetNotNilDeviceName(in.DeviceName).
		SetNotNilDeviceSecret(in.DeviceSecret).
		SetNotNilDeviceStatus(in.DeviceStatus).
		SetNotNilNickname(in.Nickname).
		SetNotNilGmtCreate(in.GmtCreate).
		SetNotNilGmtModified(in.GmtModified).
		SetNotNilUtcCreate(in.UtcCreate).
		SetNotNilUtcModified(in.UtcModified).
		SetNotNilOperationalStatusCode(in.OperationalStatusCode).
		SetNotNilFirmwareVersion(in.FirmwareVersion).
		SetNotNilGmtActive(in.GmtActive).
		SetNotNilGmtOnline(in.GmtOnline).
		SetNotNilIPAddress(in.IPAddress).
		SetNotNilNodeType(in.NodeType).
		SetNotNilOwner(in.Owner).
		SetNotNilProductName(in.ProductName).
		SetNotNilRegion(in.Region).
		SetNotNilUtcActive(in.UtcActive).
		SetNotNilUtcOnline(in.UtcOnline)

	if in.Status != nil {
		query.SetNotNilStatus(pointy.GetPointer(uint8(*in.Status)))
	}

	result, err := query.Save(l.ctx)

	if err != nil {
		return nil, dberrorhandler.DefaultEntError(l.Logger, err, in)
	}

	return &basic.BaseUUIDResp{Id: result.ID.String(), Msg: i18n.CreateSuccess}, nil
}
