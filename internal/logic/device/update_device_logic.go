package device

import (
	"context"
	"github.com/suyuan32/simple-admin-common/i18n"

	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/svc"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/utils/dberrorhandler"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/types/basic"

	"github.com/suyuan32/simple-admin-common/utils/pointy"
	"github.com/suyuan32/simple-admin-common/utils/uuidx"
	"github.com/zeromicro/go-zero/core/logx"
)

type UpdateDeviceLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewUpdateDeviceLogic(ctx context.Context, svcCtx *svc.ServiceContext) *UpdateDeviceLogic {
	return &UpdateDeviceLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *UpdateDeviceLogic) UpdateDevice(in *basic.DeviceInfo) (*basic.BaseResp, error) {
	query := l.svcCtx.DB.Device.UpdateOneID(uuidx.ParseUUIDString(*in.Id)).
		SetNotNilSort(in.Sort).
		SetNotNilProductKey(in.ProductKey).
		SetNotNilIotID(in.IotID).
		SetNotNilDeviceName(in.DeviceName).
		SetNotNilDeviceSecret(in.DeviceSecret).
		SetNotNilDeviceStatus(in.DeviceStatus).
		SetNotNilNickname(in.Nickname).
		SetNotNilGmtCreate(in.GmtCreate).
		SetNotNilGmtModified(in.GmtModified).
		SetNotNilUtcCreate(in.UtcCreate).
		SetNotNilUtcModified(in.UtcModified).
		SetNotNilOperationalStatusCode(in.OperationalStatusCode).
		SetNotNilFirmwareVersion(in.FirmwareVersion).
		SetNotNilGmtActive(in.GmtActive).
		SetNotNilGmtOnline(in.GmtOnline).
		SetNotNilIPAddress(in.IPAddress).
		SetNotNilNodeType(in.NodeType).
		SetNotNilOwner(in.Owner).
		SetNotNilProductName(in.ProductName).
		SetNotNilRegion(in.Region).
		SetNotNilUtcActive(in.UtcActive).
		SetNotNilUtcOnline(in.UtcOnline).
		SetNotNilOperationalStatus(in.OperationalStatus)

	if in.Status != nil {
		query.SetNotNilStatus(pointy.GetPointer(uint8(*in.Status)))
	}

	err := query.Exec(l.ctx)

	if err != nil {
		return nil, dberrorhandler.DefaultEntError(l.Logger, err, in)
	}

	return &basic.BaseResp{Msg: i18n.UpdateSuccess}, nil
}
