package product_tag

import (
	"context"
	"gitee.com/yuLingNet/simple-drive-aliyun/aliyun"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/ent"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/ent/product"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/svc"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/types/basic"
	"github.com/suyuan32/simple-admin-common/i18n"

	"github.com/zeromicro/go-zero/core/logx"
)

type SyncProductTagLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewSyncProductTagLogic(ctx context.Context, svcCtx *svc.ServiceContext) *SyncProductTagLogic {
	return &SyncProductTagLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *SyncProductTagLogic) SyncProductTag(in *basic.Empty) (*basic.BaseResp, error) {
	//获取产品ProductKey
	productKeys := l.svcCtx.DB.Product.Query().Select("product_key").StringsX(l.ctx)
	for _, key := range productKeys {
		//调用阿里云api
		err := ListProductTags(key, l)
		if err != nil {
			return nil, err
		}
	}

	return &basic.BaseResp{Msg: i18n.Success}, nil
}

func ListProductTags(productKey string, l *SyncProductTagLogic) error {
	productTagResult, err := aliyun.ListProductTags(productKey)
	if err != nil {
		return err
	}

	if productTagResult.Data != nil {
		list := productTagResult.Data.ProductTag
		if len(list) > 0 {
			product, _ := l.svcCtx.DB.Product.Query().Where(product.ProductKeyEQ(productKey)).First(l.ctx)
			bulk := make([]*ent.ProductTagCreate, len(list))
			for i, item := range list {
				bulk[i] = l.svcCtx.DB.ProductTag.Create().
					SetNotNilProductKey(&productKey).
					SetNotNilTagKey(item.TagKey).
					SetNotNilTagValue(item.TagValue).
					SetProduct(product)
			}
			err := l.svcCtx.DB.ProductTag.CreateBulk(bulk...).
				OnConflict().
				UpdateNewValues().
				Exec(l.ctx)
			if err != nil {
				return err
			}
		}
	}
	return nil
}
