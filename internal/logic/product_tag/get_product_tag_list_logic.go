package product_tag

import (
	"context"

	"gitee.com/yuLingNet/simple-iot-basic-rpc/ent/predicate"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/ent/producttag"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/svc"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/utils/dberrorhandler"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/types/basic"

	"github.com/suyuan32/simple-admin-common/utils/pointy"
	"github.com/zeromicro/go-zero/core/logx"
)

type GetProductTagListLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGetProductTagListLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetProductTagListLogic {
	return &GetProductTagListLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *GetProductTagListLogic) GetProductTagList(in *basic.ProductTagListReq) (*basic.ProductTagListResp, error) {
	var predicates []predicate.ProductTag
	if in.ProductKey != nil {
		predicates = append(predicates, producttag.ProductKeyContains(*in.ProductKey))
	}
	if in.TagKey != nil {
		predicates = append(predicates, producttag.TagKeyContains(*in.TagKey))
	}
	if in.TagValue != nil {
		predicates = append(predicates, producttag.TagValueContains(*in.TagValue))
	}
	result, err := l.svcCtx.DB.ProductTag.Query().Where(predicates...).Page(l.ctx, in.Page, in.PageSize)

	if err != nil {
		return nil, dberrorhandler.DefaultEntError(l.Logger, err, in)
	}

	resp := &basic.ProductTagListResp{}
	resp.Total = result.PageDetails.Total

	for _, v := range result.List {
		resp.Data = append(resp.Data, &basic.ProductTagInfo{
			Id:         &v.ID,
			CreatedAt:  pointy.GetPointer(v.CreatedAt.UnixMilli()),
			UpdatedAt:  pointy.GetPointer(v.UpdatedAt.UnixMilli()),
			Status:     pointy.GetPointer(uint32(v.Status)),
			Sort:       &v.Sort,
			ProductKey: &v.ProductKey,
			TagKey:     &v.TagKey,
			TagValue:   &v.TagValue,
		})
	}

	return resp, nil
}
