package product_tag

import (
	"context"
	"github.com/suyuan32/simple-admin-common/i18n"

	"gitee.com/yuLingNet/simple-iot-basic-rpc/ent/producttag"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/svc"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/utils/dberrorhandler"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/types/basic"

	"github.com/zeromicro/go-zero/core/logx"
)

type DeleteProductTagLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewDeleteProductTagLogic(ctx context.Context, svcCtx *svc.ServiceContext) *DeleteProductTagLogic {
	return &DeleteProductTagLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *DeleteProductTagLogic) DeleteProductTag(in *basic.IDsReq) (*basic.BaseResp, error) {
	_, err := l.svcCtx.DB.ProductTag.Delete().Where(producttag.IDIn(in.Ids...)).Exec(l.ctx)

	if err != nil {
		return nil, dberrorhandler.DefaultEntError(l.Logger, err, in)
	}

	return &basic.BaseResp{Msg: i18n.DeleteSuccess}, nil
}
