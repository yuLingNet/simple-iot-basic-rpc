package product_tag

import (
	"context"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/ent/predicate"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/ent/producttag"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/utils/dberrorhandler"
	"github.com/suyuan32/simple-admin-common/utils/pointy"

	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/svc"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/types/basic"

	"github.com/zeromicro/go-zero/core/logx"
)

type GetProductTagByKeyLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGetProductTagByKeyLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetProductTagByKeyLogic {
	return &GetProductTagByKeyLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *GetProductTagByKeyLogic) GetProductTagByKey(in *basic.ProductKeyReq) (*basic.ProductTagListResp, error) {
	var predicates []predicate.ProductTag

	predicates = append(predicates, producttag.ProductKeyContains(in.ProductKey))

	result, err := l.svcCtx.DB.ProductTag.Query().Where(predicates...).All(l.ctx)

	if err != nil {
		return nil, dberrorhandler.DefaultEntError(l.Logger, err, in)
	}

	resp := &basic.ProductTagListResp{}
	resp.Total = uint64(len(result))

	for _, v := range result {
		resp.Data = append(resp.Data, &basic.ProductTagInfo{
			Id:         &v.ID,
			CreatedAt:  pointy.GetPointer(v.CreatedAt.UnixMilli()),
			UpdatedAt:  pointy.GetPointer(v.UpdatedAt.UnixMilli()),
			Status:     pointy.GetPointer(uint32(v.Status)),
			Sort:       &v.Sort,
			ProductKey: &v.ProductKey,
			TagKey:     &v.TagKey,
			TagValue:   &v.TagValue,
		})
	}

	return resp, nil
}
