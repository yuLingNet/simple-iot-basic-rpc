package product_service

import (
	"context"

	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/svc"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/utils/dberrorhandler"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/types/basic"

	"github.com/suyuan32/simple-admin-common/utils/pointy"
	"github.com/zeromicro/go-zero/core/logx"
)

type GetProductServiceByIdLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGetProductServiceByIdLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetProductServiceByIdLogic {
	return &GetProductServiceByIdLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *GetProductServiceByIdLogic) GetProductServiceById(in *basic.IDReq) (*basic.ProductServiceInfo, error) {
	result, err := l.svcCtx.DB.ProductService.Get(l.ctx, in.Id)
	if err != nil {
		return nil, dberrorhandler.DefaultEntError(l.Logger, err, in)
	}

	return &basic.ProductServiceInfo{
		Id:          &result.ID,
		CreatedAt:    pointy.GetPointer(result.CreatedAt.UnixMilli()),
		UpdatedAt:    pointy.GetPointer(result.UpdatedAt.UnixMilli()),
		Status:	pointy.GetPointer(uint32(result.Status)),
		Sort:	&result.Sort,
		ProductKey:	&result.ProductKey,
		CreateTs:	&result.CreateTs,
		Version:	&result.Version,
		Identifier:	&result.Identifier,
		ServiceName:	&result.ServiceName,
		InputParams:	&result.InputParams,
		OutputParams:	&result.OutputParams,
		Required:	&result.Required,
		CallType:	&result.CallType,
		Custom:	&result.Custom,
	}, nil
}

