package product_service

import (
	"context"
	"github.com/suyuan32/simple-admin-common/i18n"

	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/svc"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/utils/dberrorhandler"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/types/basic"

	"github.com/suyuan32/simple-admin-common/utils/pointy"
	"github.com/zeromicro/go-zero/core/logx"
)

type CreateProductServiceLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewCreateProductServiceLogic(ctx context.Context, svcCtx *svc.ServiceContext) *CreateProductServiceLogic {
	return &CreateProductServiceLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *CreateProductServiceLogic) CreateProductService(in *basic.ProductServiceInfo) (*basic.BaseIDResp, error) {
	query := l.svcCtx.DB.ProductService.Create().
		SetNotNilSort(in.Sort).
		SetNotNilProductKey(in.ProductKey).
		SetNotNilCreateTs(in.CreateTs).
		SetNotNilVersion(in.Version).
		SetNotNilIdentifier(in.Identifier).
		SetNotNilServiceName(in.ServiceName).
		SetNotNilInputParams(in.InputParams).
		SetNotNilOutputParams(in.OutputParams).
		SetNotNilRequired(in.Required).
		SetNotNilCallType(in.CallType).
		SetNotNilCustom(in.Custom)

	if in.Status != nil {
		query.SetNotNilStatus(pointy.GetPointer(uint8(*in.Status)))
	}

	result, err := query.Save(l.ctx)

	if err != nil {
		return nil, dberrorhandler.DefaultEntError(l.Logger, err, in)
	}

	return &basic.BaseIDResp{Id: result.ID, Msg: i18n.CreateSuccess}, nil
}
