package product_service

import (
	"context"
	"github.com/suyuan32/simple-admin-common/i18n"

	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/svc"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/utils/dberrorhandler"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/types/basic"

	"github.com/suyuan32/simple-admin-common/utils/pointy"
	"github.com/zeromicro/go-zero/core/logx"
)

type UpdateProductServiceLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewUpdateProductServiceLogic(ctx context.Context, svcCtx *svc.ServiceContext) *UpdateProductServiceLogic {
	return &UpdateProductServiceLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *UpdateProductServiceLogic) UpdateProductService(in *basic.ProductServiceInfo) (*basic.BaseResp, error) {
	query := l.svcCtx.DB.ProductService.UpdateOneID(*in.Id).
		SetNotNilSort(in.Sort).
		SetNotNilProductKey(in.ProductKey).
		SetNotNilCreateTs(in.CreateTs).
		SetNotNilVersion(in.Version).
		SetNotNilIdentifier(in.Identifier).
		SetNotNilServiceName(in.ServiceName).
		SetNotNilInputParams(in.InputParams).
		SetNotNilOutputParams(in.OutputParams).
		SetNotNilRequired(in.Required).
		SetNotNilCallType(in.CallType).
		SetNotNilCustom(in.Custom)

	if in.Status != nil {
		query.SetNotNilStatus(pointy.GetPointer(uint8(*in.Status)))
	}

	err := query.Exec(l.ctx)

	if err != nil {
		return nil, dberrorhandler.DefaultEntError(l.Logger, err, in)
	}

	return &basic.BaseResp{Msg: i18n.UpdateSuccess}, nil
}
