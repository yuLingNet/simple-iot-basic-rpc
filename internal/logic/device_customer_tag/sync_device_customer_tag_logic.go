package device_customer_tag

import (
	"context"
	"fmt"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/ent"
	"github.com/jinzhu/gorm"
	"log"

	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/svc"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/types/basic"

	_ "github.com/jinzhu/gorm/dialects/mssql"
	"github.com/zeromicro/go-zero/core/logx"
)

type Result struct {
	Sn       string
	Customer string
	City     string
	Location string
}

type SyncDeviceCustomerTagLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewSyncDeviceCustomerTagLogic(ctx context.Context, svcCtx *svc.ServiceContext) *SyncDeviceCustomerTagLogic {
	return &SyncDeviceCustomerTagLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *SyncDeviceCustomerTagLogic) SyncDeviceCustomerTag(in *basic.Empty) (*basic.BaseResp, error) {
	/*
	*连接mssql
	 */
	connectionString := fmt.Sprintf("server=%s;user id=%s;password=%s;database=%s", "192.168.105.198", "yuling", "yuLing@#2024", "abascnzs")
	db, err := gorm.Open("mssql", connectionString)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	devices, _ := l.svcCtx.DB.Device.Query().All(context.Background())
	var bulk []*ent.DeviceCustomerTagCreate
	var line2 string
	for i, d := range devices {
		log.Println(i, ":deviceName = ", d.DeviceName)
		var result []Result
		db.Raw("SELECT Top 1 SN_No as sn, customer_E as customer,city,location FROM MK_IOT_CUSTOMER_TAG where SN_No = '" + d.DeviceName + "'").Scan(&result)
		for _, r := range result {
			if r.Location != "" {
				line2 = r.Location + " " + r.Sn
			} else {
				line2 = r.Sn
			}
			bulk = append(bulk, l.svcCtx.DB.DeviceCustomerTag.Create().
				SetIotID(d.IotID).
				SetDeviceName(d.DeviceName).
				SetSn(r.Sn).
				SetCustomer(r.Customer).
				SetCity(r.City).
				SetLocation(r.Location).
				SetLine1(r.Customer+"_"+r.City).
				SetLine2(line2))
		}
	}
	err = l.svcCtx.DB.DeviceCustomerTag.CreateBulk(bulk...).
		OnConflict().
		UpdateNewValues().
		Exec(context.Background())
	if err != nil {
		log.Fatal(err)
	}

	return &basic.BaseResp{}, nil
}
