package device_customer_tag

import (
	"context"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/ent/devicecustomertag"
	"github.com/suyuan32/simple-admin-common/utils/pointy"

	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/svc"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/types/basic"

	"github.com/zeromicro/go-zero/core/logx"
)

type GetDeviceCustomerTagByIotIdLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGetDeviceCustomerTagByIotIdLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetDeviceCustomerTagByIotIdLogic {
	return &GetDeviceCustomerTagByIotIdLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *GetDeviceCustomerTagByIotIdLogic) GetDeviceCustomerTagByIotId(in *basic.DeviceIotIdReq) (*basic.DeviceCustomerTagInfo, error) {
	result, err := l.svcCtx.DB.DeviceCustomerTag.Query().Where(devicecustomertag.IotIDEQ(in.IotId)).First(l.ctx)
	if err != nil {
		//return nil, dberrorhandler.DefaultEntError(l.Logger, err, in)
		return &basic.DeviceCustomerTagInfo{}, nil
	}

	return &basic.DeviceCustomerTagInfo{
		Id:         &result.ID,
		CreatedAt:  pointy.GetPointer(result.CreatedAt.UnixMilli()),
		UpdatedAt:  pointy.GetPointer(result.UpdatedAt.UnixMilli()),
		Status:     pointy.GetPointer(uint32(result.Status)),
		Sort:       &result.Sort,
		IotID:      &result.IotID,
		DeviceName: &result.DeviceName,
		Sn:         &result.Sn,
		Customer:   &result.Customer,
		City:       &result.City,
		Location:   &result.Location,
		Line1:      &result.Line1,
		Line2:      &result.Line2,
	}, nil
}
