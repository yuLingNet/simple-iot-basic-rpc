package bind

import (
	"context"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/ent"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/ent/member"
	"github.com/suyuan32/simple-admin-common/utils/pointy"
	"github.com/suyuan32/simple-admin-common/utils/uuidx"
	"strings"

	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/svc"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/types/basic"

	"github.com/zeromicro/go-zero/core/logx"
)

type GetBindMemberListLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGetBindMemberListLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetBindMemberListLogic {
	return &GetBindMemberListLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *GetBindMemberListLogic) GetBindMemberList(in *basic.BindMemberListReq) (*basic.DeviceListResp, error) {
	memberInfo, _ := l.svcCtx.DB.Member.Query().Where(member.IDEQ(uuidx.ParseUUIDString(in.Id))).First(l.ctx)
	devices, _ := memberInfo.QueryDevices().Where().Page(l.ctx, in.Page, in.PageSize, func(pager *ent.DevicePager) {
		if in.Prop != nil && in.Order != nil {
			if *in.Order == "descending" {
				pager.Order = ent.Desc(camelCaseToUnderscore(*in.Prop))
			} else {
				pager.Order = ent.Asc(camelCaseToUnderscore(*in.Prop))
			}
		}
	})
	resp := &basic.DeviceListResp{}
	resp.Total = devices.PageDetails.Total

	for _, v := range devices.List {
		resp.Data = append(resp.Data, &basic.DeviceInfo{
			Id:                    pointy.GetPointer(v.ID.String()),
			CreatedAt:             pointy.GetPointer(v.CreatedAt.UnixMilli()),
			UpdatedAt:             pointy.GetPointer(v.UpdatedAt.UnixMilli()),
			Status:                pointy.GetPointer(uint32(v.Status)),
			ProductKey:            &v.ProductKey,
			IotID:                 &v.IotID,
			DeviceName:            &v.DeviceName,
			DeviceSecret:          &v.DeviceSecret,
			DeviceStatus:          &v.DeviceStatus,
			Nickname:              &v.Nickname,
			GmtCreate:             &v.GmtCreate,
			GmtModified:           &v.GmtModified,
			UtcCreate:             &v.UtcCreate,
			UtcModified:           &v.UtcModified,
			OperationalStatusCode: &v.OperationalStatusCode,
			FirmwareVersion:       &v.FirmwareVersion,
			GmtActive:             &v.GmtActive,
			GmtOnline:             &v.GmtOnline,
			IPAddress:             &v.IPAddress,
			NodeType:              &v.NodeType,
			Owner:                 &v.Owner,
			ProductName:           &v.ProductName,
			Region:                &v.Region,
			UtcActive:             &v.UtcActive,
			UtcOnline:             &v.UtcOnline,
		})
	}

	return resp, nil

}

func camelCaseToUnderscore(s string) string {
	if s == "iotID" {
		return "iot_id"
	} else {
		data := make([]byte, 0, len(s)*2)
		j := false
		num := len(s)
		for i := 0; i < num; i++ {
			d := s[i]
			// or通过ASCII码进行大小写的转化
			// 65-90（A-Z），97-122（a-z）
			//判断如果字母为大写的A-Z就在前面拼接一个_
			if i > 0 && d >= 'A' && d <= 'Z' && j {
				data = append(data, '_')
			}
			if d != '_' {
				j = true
			}
			data = append(data, d)
		}
		//ToLower把大写字母统一转小写
		return strings.ToLower(string(data[:]))
	}
}
