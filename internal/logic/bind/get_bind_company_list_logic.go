package bind

import (
	"context"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/ent"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/ent/company"
	"github.com/suyuan32/simple-admin-common/utils/pointy"

	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/svc"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/types/basic"

	"github.com/zeromicro/go-zero/core/logx"
)

type GetBindCompanyListLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGetBindCompanyListLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetBindCompanyListLogic {
	return &GetBindCompanyListLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *GetBindCompanyListLogic) GetBindCompanyList(in *basic.BindCompanyListReq) (*basic.DeviceListResp, error) {
	companyInfo, _ := l.svcCtx.DB.Company.Query().Where(company.IDEQ(in.Id)).First(l.ctx)
	devices, _ := companyInfo.QueryDevices().Where().Page(l.ctx, in.Page, in.PageSize, func(pager *ent.DevicePager) {
		if in.Prop != nil && in.Order != nil {
			if *in.Order == "descending" {
				pager.Order = ent.Desc(camelCaseToUnderscore(*in.Prop))
			} else {
				pager.Order = ent.Asc(camelCaseToUnderscore(*in.Prop))
			}
		}
	})
	resp := &basic.DeviceListResp{}
	resp.Total = devices.PageDetails.Total

	for _, v := range devices.List {
		resp.Data = append(resp.Data, &basic.DeviceInfo{
			Id:                    pointy.GetPointer(v.ID.String()),
			CreatedAt:             pointy.GetPointer(v.CreatedAt.UnixMilli()),
			UpdatedAt:             pointy.GetPointer(v.UpdatedAt.UnixMilli()),
			Status:                pointy.GetPointer(uint32(v.Status)),
			ProductKey:            &v.ProductKey,
			IotID:                 &v.IotID,
			DeviceName:            &v.DeviceName,
			DeviceSecret:          &v.DeviceSecret,
			DeviceStatus:          &v.DeviceStatus,
			Nickname:              &v.Nickname,
			GmtCreate:             &v.GmtCreate,
			GmtModified:           &v.GmtModified,
			UtcCreate:             &v.UtcCreate,
			UtcModified:           &v.UtcModified,
			OperationalStatusCode: &v.OperationalStatusCode,
			FirmwareVersion:       &v.FirmwareVersion,
			GmtActive:             &v.GmtActive,
			GmtOnline:             &v.GmtOnline,
			IPAddress:             &v.IPAddress,
			NodeType:              &v.NodeType,
			Owner:                 &v.Owner,
			ProductName:           &v.ProductName,
			Region:                &v.Region,
			UtcActive:             &v.UtcActive,
			UtcOnline:             &v.UtcOnline,
		})
	}

	return resp, nil

}
