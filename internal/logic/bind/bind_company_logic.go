package bind

import (
	"context"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/ent"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/ent/device"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/svc"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/utils/dberrorhandler"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/utils/entx"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/types/basic"
	"github.com/suyuan32/simple-admin-common/i18n"

	"github.com/zeromicro/go-zero/core/logx"
)

type BindCompanyLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewBindCompanyLogic(ctx context.Context, svcCtx *svc.ServiceContext) *BindCompanyLogic {
	return &BindCompanyLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *BindCompanyLogic) BindCompany(in *basic.BindCompanyReq) (*basic.BaseResp, error) {
	devices, err := l.svcCtx.DB.Device.Query().Where(device.IotIDIn(in.IotIds...)).All(l.ctx)
	if err != nil {
		return nil, dberrorhandler.DefaultEntError(l.Logger, err, in)
	}

	err = entx.WithTx(l.ctx, l.svcCtx.DB, func(tx *ent.Tx) error {
		updateQuery := tx.Company.UpdateOneID(in.Id).
			AddDevices(devices...)

		return updateQuery.Exec(l.ctx)
	})
	if err != nil {
		return nil, dberrorhandler.DefaultEntError(l.Logger, err, in)
	}

	return &basic.BaseResp{Msg: i18n.Success}, nil
}
