package product_event

import (
	"context"

	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/svc"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/utils/dberrorhandler"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/types/basic"

	"github.com/suyuan32/simple-admin-common/utils/pointy"
	"github.com/zeromicro/go-zero/core/logx"
)

type GetProductEventByIdLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGetProductEventByIdLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetProductEventByIdLogic {
	return &GetProductEventByIdLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *GetProductEventByIdLogic) GetProductEventById(in *basic.IDReq) (*basic.ProductEventInfo, error) {
	result, err := l.svcCtx.DB.ProductEvent.Get(l.ctx, in.Id)
	if err != nil {
		return nil, dberrorhandler.DefaultEntError(l.Logger, err, in)
	}

	return &basic.ProductEventInfo{
		Id:          &result.ID,
		CreatedAt:    pointy.GetPointer(result.CreatedAt.UnixMilli()),
		UpdatedAt:    pointy.GetPointer(result.UpdatedAt.UnixMilli()),
		Status:	pointy.GetPointer(uint32(result.Status)),
		Sort:	&result.Sort,
		ProductKey:	&result.ProductKey,
		CreateTs:	&result.CreateTs,
		Version:	&result.Version,
		Identifier:	&result.Identifier,
		EventName:	&result.EventName,
		EventType:	&result.EventType,
		OutputData:	&result.OutputData,
		Custom:	&result.Custom,
		Required:	&result.Required,
	}, nil
}

