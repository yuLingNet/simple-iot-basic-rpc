package product_event

import (
	"context"
	"github.com/suyuan32/simple-admin-common/i18n"

	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/svc"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/utils/dberrorhandler"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/types/basic"

	"github.com/suyuan32/simple-admin-common/utils/pointy"
	"github.com/zeromicro/go-zero/core/logx"
)

type UpdateProductEventLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewUpdateProductEventLogic(ctx context.Context, svcCtx *svc.ServiceContext) *UpdateProductEventLogic {
	return &UpdateProductEventLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *UpdateProductEventLogic) UpdateProductEvent(in *basic.ProductEventInfo) (*basic.BaseResp, error) {
	query := l.svcCtx.DB.ProductEvent.UpdateOneID(*in.Id).
		SetNotNilSort(in.Sort).
		SetNotNilProductKey(in.ProductKey).
		SetNotNilCreateTs(in.CreateTs).
		SetNotNilVersion(in.Version).
		SetNotNilIdentifier(in.Identifier).
		SetNotNilEventName(in.EventName).
		SetNotNilEventType(in.EventType).
		SetNotNilOutputData(in.OutputData).
		SetNotNilCustom(in.Custom).
		SetNotNilRequired(in.Required)

	if in.Status != nil {
		query.SetNotNilStatus(pointy.GetPointer(uint8(*in.Status)))
	}

	err := query.Exec(l.ctx)

	if err != nil {
		return nil, dberrorhandler.DefaultEntError(l.Logger, err, in)
	}

	return &basic.BaseResp{Msg: i18n.UpdateSuccess}, nil
}
