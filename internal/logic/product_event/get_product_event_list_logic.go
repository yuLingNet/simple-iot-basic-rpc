package product_event

import (
	"context"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/ent"
	"strings"

	"gitee.com/yuLingNet/simple-iot-basic-rpc/ent/predicate"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/ent/productevent"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/svc"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/internal/utils/dberrorhandler"
	"gitee.com/yuLingNet/simple-iot-basic-rpc/types/basic"

	"github.com/suyuan32/simple-admin-common/utils/pointy"
	"github.com/zeromicro/go-zero/core/logx"
)

type GetProductEventListLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGetProductEventListLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetProductEventListLogic {
	return &GetProductEventListLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *GetProductEventListLogic) GetProductEventList(in *basic.ProductEventListReq) (*basic.ProductEventListResp, error) {
	var predicates []predicate.ProductEvent
	if in.ProductKey != nil {
		predicates = append(predicates, productevent.ProductKeyContains(*in.ProductKey))
	}
	if in.Version != nil {
		predicates = append(predicates, productevent.VersionContains(*in.Version))
	}
	if in.Identifier != nil {
		predicates = append(predicates, productevent.IdentifierContains(*in.Identifier))
	}
	result, err := l.svcCtx.DB.ProductEvent.Query().Where(predicates...).Page(l.ctx, in.Page, in.PageSize, func(pager *ent.ProductEventPager) {
		if in.Prop != nil && in.Order != nil {
			if *in.Order == "descending" {
				pager.Order = ent.Desc(camelCaseToUnderscore(*in.Prop))
			} else {
				pager.Order = ent.Asc(camelCaseToUnderscore(*in.Prop))
			}
		}
	})

	if err != nil {
		return nil, dberrorhandler.DefaultEntError(l.Logger, err, in)
	}

	resp := &basic.ProductEventListResp{}
	resp.Total = result.PageDetails.Total

	for _, v := range result.List {
		resp.Data = append(resp.Data, &basic.ProductEventInfo{
			Id:         &v.ID,
			CreatedAt:  pointy.GetPointer(v.CreatedAt.UnixMilli()),
			UpdatedAt:  pointy.GetPointer(v.UpdatedAt.UnixMilli()),
			Status:     pointy.GetPointer(uint32(v.Status)),
			Sort:       &v.Sort,
			ProductKey: &v.ProductKey,
			CreateTs:   &v.CreateTs,
			Version:    &v.Version,
			Identifier: &v.Identifier,
			EventName:  &v.EventName,
			EventType:  &v.EventType,
			OutputData: &v.OutputData,
			Custom:     &v.Custom,
			Required:   &v.Required,
		})
	}

	return resp, nil
}

func camelCaseToUnderscore(s string) string {
	data := make([]byte, 0, len(s)*2)
	j := false
	num := len(s)
	for i := 0; i < num; i++ {
		d := s[i]
		// or通过ASCII码进行大小写的转化
		// 65-90（A-Z），97-122（a-z）
		//判断如果字母为大写的A-Z就在前面拼接一个_
		if i > 0 && d >= 'A' && d <= 'Z' && j {
			data = append(data, '_')
		}
		if d != '_' {
			j = true
		}
		data = append(data, d)
	}
	//ToLower把大写字母统一转小写
	return strings.ToLower(string(data[:]))
}
